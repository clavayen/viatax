<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 15/11/18
 * Time: 12:05
 */

namespace App\Exception;


class ProtectorPendingAcceptanceException extends \Exception {
    /**
     * ProtectorPendingAcceptanceException constructor.
     */
    public function __construct($message=null)
    {
        parent::__construct($message);
    }

}