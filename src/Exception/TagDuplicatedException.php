<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 09/11/18
 * Time: 10:07
 */

namespace App\Exception;


class TagDuplicatedException extends \Exception {
    public function __construct($message=null)
    {
        parent::__construct($message);
    }
}