<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 09/11/18
 * Time: 09:45
 */

namespace App\Exception;


class PatentDuplicatedException extends \Exception {
    /**
     * PatentDuplicatedException constructor.
     */
    public function __construct($message=null)
    {
        parent::__construct($message);
    }
}