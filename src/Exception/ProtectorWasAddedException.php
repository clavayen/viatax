<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 15/11/18
 * Time: 11:40
 */

namespace App\Exception;


class ProtectorWasAddedException extends \Exception
{
    /**
     * EmailDuplicatedException constructor.
     */
    public function __construct($message=null)
    {
        parent::__construct($message);
    }

}