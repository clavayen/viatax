<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 09/11/18
 * Time: 08:18
 */

namespace App\Exception;


class NameDuplicatedException extends \Exception {

    public function __construct($message=null)
    {
        parent::__construct($message);
    }

}