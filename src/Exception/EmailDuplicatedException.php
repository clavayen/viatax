<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 09/11/18
 * Time: 08:23
 */

namespace App\Exception;


class EmailDuplicatedException extends \Exception {

    /**
     * EmailDuplicatedException constructor.
     */
    public function __construct($message=null)
    {
        parent::__construct($message);
    }
}