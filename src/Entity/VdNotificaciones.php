<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdNotificaciones
 *
 * @ORM\Table(name="vd_notificaciones")
 * @ORM\Entity
 */
class VdNotificaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="notificacion_recibida", type="boolean", nullable=true, options={"default"="1","comment"="1: NO RECIBIDA, 2: RECIBIDA"})
     */
    private $notificacionRecibida = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=true)
     */
    private $eliminado;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion = 'CURRENT_TIMESTAMP';

    /**
     * @var \VdViajes
     *
     * @ORM\ManyToOne(targetEntity="VdViajes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viaje_id", referencedColumnName="id")
     * })
     */
    private $viaje;

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="protector_id", referencedColumnName="id")
     * })
     */
    private $protector;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotificacionRecibida(): ?bool
    {
        return $this->notificacionRecibida;
    }

    public function setNotificacionRecibida(?bool $notificacionRecibida): self
    {
        $this->notificacionRecibida = $notificacionRecibida;

        return $this;
    }

    public function getEliminado(): ?bool
    {
        return $this->eliminado;
    }

    public function setEliminado(?bool $eliminado): self
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getViaje(): ?VdViajes
    {
        return $this->viaje;
    }

    public function setViaje(?VdViajes $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }

    public function getProtector(): ?VdUsuarios
    {
        return $this->protector;
    }

    public function setProtector(?VdUsuarios $protector): self
    {
        $this->protector = $protector;

        return $this;
    }


}
