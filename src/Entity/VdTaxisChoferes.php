<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdTaxisChoferes
 *
 * @ORM\Table(name="vd_taxis_choferes")
 * @ORM\Entity
 */
class VdTaxisChoferes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \VdTaxis
     *
     * @ORM\ManyToOne(targetEntity="VdTaxis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxi_id", referencedColumnName="id")
     * })
     */
    private $taxi;

    /**
     * @var \VdChoferes
     *
     * @ORM\ManyToOne(targetEntity="VdChoferes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chofer_id", referencedColumnName="id")
     * })
     */
    private $chofer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaxi(): ?VdTaxis
    {
        return $this->taxi;
    }

    public function setTaxi(?VdTaxis $taxi): self
    {
        $this->taxi = $taxi;

        return $this;
    }

    public function getChofer(): ?VdChoferes
    {
        return $this->chofer;
    }

    public function setChofer(?VdChoferes $chofer): self
    {
        $this->chofer = $chofer;

        return $this;
    }


}
