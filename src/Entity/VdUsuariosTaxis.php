<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdUsuariosTaxis
 *
 * @ORM\Table(name="vd_usuarios_taxis")
 * @ORM\Entity(repositoryClass="App\Repository\UserTaxiRepository")
 */
class VdUsuariosTaxis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \VdTaxis
     *
     * @ORM\ManyToOne(targetEntity="VdTaxis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxi_id", referencedColumnName="id")
     * })
     */
    private $taxi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuario(): ?VdUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?VdUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getTaxi(): ?VdTaxis
    {
        return $this->taxi;
    }

    public function setTaxi(?VdTaxis $taxi): self
    {
        $this->taxi = $taxi;

        return $this;
    }


}
