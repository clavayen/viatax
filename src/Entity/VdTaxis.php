<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdTaxis
 *
 * @ORM\Table(name="vd_taxis")
 * @ORM\Entity(repositoryClass="App\Repository\TaxiRepository")
 */
class VdTaxis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="patente", type="string", length=45, nullable=false)
     */
    private $patente;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string", length=100, nullable=false)
     */
    private $modelo;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=100, nullable=false)
     */
    private $marca;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresa", type="string", length=100, nullable=true)
     */
    private $empresa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false)
     */
    private $fechaCreacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tag", type="string", length=100, nullable=true)
     */
    private $tag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="anio", type="string", length=8, nullable=true)
     */
    private $anio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $qualification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPatente(): ?string
    {
        return $this->patente;
    }

    public function setPatente(string $patente): self
    {
        $this->patente = $patente;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getMarca(): ?string
    {
        return $this->marca;
    }

    public function setMarca(string $marca): self
    {
        $this->marca = $marca;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEmpresa(): ?string
    {
        return $this->empresa;
    }

    public function setEmpresa(?string $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param null|string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return null|string
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * @param null|string $anio
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;
    }

    public function getQualification()
    {
        return $this->qualification;
    }

    public function setQualification( $qualification = null)
    {
        $this->qualification = $qualification;
    }

}
