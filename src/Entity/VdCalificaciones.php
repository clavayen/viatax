<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdCalificaciones
 *
 * @ORM\Table(name="vd_calificaciones")
 * @ORM\Entity(repositoryClass="App\Repository\CalificationRepository")
 */
class VdCalificaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cortesia", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $cortesia;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_vehiculo", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $estadoVehiculo;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta_seleccionada", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $rutaSeleccionada;

    /**
     * @var string
     *
     * @ORM\Column(name="conduccion", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $conduccion;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $total;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fecha;

    /**
     * @var \VdViajes
     *
     * @ORM\OneToOne(targetEntity="VdViajes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viaje_id", referencedColumnName="id")
     * })
     */
    private $viaje;


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCortesia(): string
    {
        return $this->cortesia;
    }

    /**
     * @param string $cortesia
     */
    public function setCortesia(string $cortesia): void
    {
        $this->cortesia = $cortesia;
    }

    /**
     * @return string
     */
    public function getEstadoVehiculo(): string
    {
        return $this->estadoVehiculo;
    }

    /**
     * @param string $estadoVehiculo
     */
    public function setEstadoVehiculo(string $estadoVehiculo): void
    {
        $this->estadoVehiculo = $estadoVehiculo;
    }

    /**
     * @return string
     */
    public function getRutaSeleccionada(): string
    {
        return $this->rutaSeleccionada;
    }

    /**
     * @param string $rutaSeleccionada
     */
    public function setRutaSeleccionada(string $rutaSeleccionada): void
    {
        $this->rutaSeleccionada = $rutaSeleccionada;
    }

    /**
     * @return string
     */
    public function getConduccion(): string
    {
        return $this->conduccion;
    }

    /**
     * @param string $conduccion
     */
    public function setConduccion(string $conduccion): void
    {
        $this->conduccion = $conduccion;
    }

    /**
     * @return string
     */
    public function getTotal(): string
    {
        return $this->total;
    }

    /**
     * @param string $total
     */
    public function setTotal(string $total): void
    {
        $this->total = $total;
    }

    public function getViaje(): ?VdViajes
    {
        return $this->viaje;
    }

    public function setViaje(?VdViajes $viaje): self
    {
        $this->viaje = $viaje;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getFecha(): ?\DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime|null $fecha
     */
    public function setFecha(?\DateTime $fecha): void
    {
        $this->fecha = $fecha;
    }


}
