<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdUsuariosProtectores
 *
 * @ORM\Table(name="vd_usuarios_protectores")
 * @ORM\Entity(repositoryClass="App\Repository\UserProtectorRepository")
 */
class VdUsuariosProtectores
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="protector_id", referencedColumnName="id")
     * })
     */
    private $protector;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_aceptacion", type="datetime", nullable=true)
     */
    private $fechaAceptacion;

    /**
     * @var string
     *
     * @ORM\Column(name="relacion", type="string", length=100, nullable=false)
     */
    private $relacion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuario(): ?VdUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?VdUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getProtector(): ?VdUsuarios
    {
        return $this->protector;
    }

    public function setProtector(?VdUsuarios $protector): self
    {
        $this->protector = $protector;

        return $this;
    }

    /**
     * @return string
     */
    public function getRelacion()
    {
        return $this->relacion;
    }

    /**
     * @param string $relacion
     */
    public function setRelacion($relacion)
    {
        $this->relacion = $relacion;
    }

    /**
     * @return bool|null
     */
    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    /**
     * @param bool|null $estado
     */
    public function setEstado(?bool $estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param \DateTime $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    /**
     * @return \DateTime
     */
    public function getFechaAceptacion()
    {
        return $this->fechaAceptacion;
    }

    /**
     * @param \DateTime $fechaAceptacion
     */
    public function setFechaAceptacion($fechaAceptacion)
    {
        $this->fechaAceptacion = $fechaAceptacion;
    }



}
