<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdViajes
 *
 * @ORM\Table(name="vd_viajes")
 * @ORM\Entity(repositoryClass="App\Repository\TravelRepository")
 */
class VdViajes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nro_viaje", type="string", length=45, nullable=false)
     */
    private $nroViaje;

    /**
     * @var string
     *
     * @ORM\Column(name="latitud_inicio", type="string", length=45, nullable=false)
     */
    private $latitudInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="longitud_inicio", type="string", length=45, nullable=false)
     */
    private $longitudInicio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="latitud_fin", type="string", length=45, nullable=true)
     */
    private $latitudFin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="longitud_fin", type="string", length=45, nullable=true)
     */
    private $longitudFin;

    /**
     * @var string
     *
     * @ORM\Column(name="canal_socket", type="string", length=45, nullable=false)
     */
    private $canalSocket;

    /**
     * @var string|null
     *
     * @ORM\Column(name="calificacion", type="string", length=45, nullable=true)
     */
    private $calificacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cantidad_avisos", type="integer", nullable=true, options={"comment"="es para saber la cantidad de avisos que emitio la aplicacion para saber si termino el viaje"})
     */
    private $cantidadAvisos;

    /**
     * @var \VdEstadosViajes
     *
     * @ORM\ManyToOne(targetEntity="VdEstadosViajes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_viaje_id", referencedColumnName="id")
     * })
     */
    private $estadoViaje;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion = 'CURRENT_TIMESTAMP';

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \VdTaxis
     *
     * @ORM\ManyToOne(targetEntity="VdTaxis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxi_id", referencedColumnName="id")
     * })
     */
    private $taxi;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=150, nullable=true)
     */
    private $descripcion;

    /**
     * @var \VdLugaresFrecuentes
     *
     * @ORM\ManyToOne(targetEntity="VdLugaresFrecuentes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lugar_frecuente_id", referencedColumnName="id")
     * })
     */
    private $lugarFrecuente;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_finalizacion", type="datetime", nullable=true)
     */
    private $fechaFinalizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="string", length=45, nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="tiempo_estimado", type="string", length=45, nullable=true)
     */
    private $tiempoEstimado;

    /**
     * @var string
     *
     * @ORM\Column(name="distancia_estimada", type="string", length=45, nullable=true)
     */
    private $distanciaEstimada;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $coordenadas;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="VdCalificaciones", mappedBy="viaje")
     */
    private $calificacionViaje;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNroViaje(): ?string
    {
        return $this->nroViaje;
    }

    public function setNroViaje(string $nroViaje): self
    {
        $this->nroViaje = $nroViaje;

        return $this;
    }

    public function getLatitudInicio(): ?string
    {
        return $this->latitudInicio;
    }

    public function setLatitudInicio(string $latitudInicio): self
    {
        $this->latitudInicio = $latitudInicio;

        return $this;
    }

    public function getLongitudInicio(): ?string
    {
        return $this->longitudInicio;
    }

    public function setLongitudInicio(string $longitudInicio): self
    {
        $this->longitudInicio = $longitudInicio;

        return $this;
    }

    public function getLatitudFin(): ?string
    {
        return $this->latitudFin;
    }

    public function setLatitudFin(?string $latitudFin): self
    {
        $this->latitudFin = $latitudFin;

        return $this;
    }

    public function getLongitudFin(): ?string
    {
        return $this->longitudFin;
    }

    public function setLongitudFin(?string $longitudFin): self
    {
        $this->longitudFin = $longitudFin;

        return $this;
    }

    public function getCanalSocket(): ?string
    {
        return $this->canalSocket;
    }

    public function setCanalSocket(string $canalSocket): self
    {
        $this->canalSocket = $canalSocket;

        return $this;
    }

    public function getCalificacion(): ?string
    {
        return $this->calificacion;
    }

    public function setCalificacion(?string $calificacion): self
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    public function getCantidadAvisos(): ?int
    {
        return $this->cantidadAvisos;
    }

    public function setCantidadAvisos(?int $cantidadAvisos): self
    {
        $this->cantidadAvisos = $cantidadAvisos;

        return $this;
    }

    public function getEstadoViaje(): ?VdEstadosViajes
    {
        return $this->estadoViaje;
    }

    public function setEstadoViaje(?VdEstadosViajes $estadoViaje): self
    {
        $this->estadoViaje = $estadoViaje;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getUsuario(): ?VdUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?VdUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getTaxi(): ?VdTaxis
    {
        return $this->taxi;
    }

    public function setTaxi(?VdTaxis $taxi): self
    {
        $this->taxi = $taxi;

        return $this;
    }

    /**
     * @return \VdLugaresFrecuentes
     */
    public function getLugarFrecuente()
    {
        return $this->lugarFrecuente;
    }

    /**
     * @param \VdLugaresFrecuentes $lugarFrecuente
     */
    public function setLugarFrecuente($lugarFrecuente)
    {
        $this->lugarFrecuente = $lugarFrecuente;
    }



    /**
     * @return null|string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return \DateTime|null
     */
    public function getFechaFinalizacion()
    {
        return $this->fechaFinalizacion;
    }

    /**
     * @param \DateTime|null $fechaFinalizacion
     */
    public function setFechaFinalizacion($fechaFinalizacion)
    {
        $this->fechaFinalizacion = $fechaFinalizacion;
    }

    public function setCoordenadas($coordenadas = null) {
        $this->coordenadas = $coordenadas;
        return $this;
    }

    public function getCoordenadas() {
        return $this->coordenadas;
    }

    public function setCalificacionViaje($calificacionViaje = null) {
        $this->calificacionViaje = $calificacionViaje;
        return $this;
    }

    public function getCalificacionViaje() {
        return $this->calificacionViaje;
    }



    /**
     * @return string
     */
    public function getPrecio(): string
    {
        return $this->precio;
    }

    /**
     * @param string $precio
     */
    public function setPrecio(string $precio): void
    {
        $this->precio = $precio;
    }

    /**
     * @return string
     */
    public function getTiempoEstimado(): string
    {
        return $this->tiempoEstimado;
    }

    /**
     * @param string $tiempoEstimado
     */
    public function setTiempoEstimado(string $tiempoEstimado): void
    {
        $this->tiempoEstimado = $tiempoEstimado;
    }

    /**
     * @return string
     */
    public function getDistanciaEstimada(): string
    {
        return $this->distanciaEstimada;
    }

    /**
     * @param string $distanciaEstimada
     */
    public function setDistanciaEstimada(string $distanciaEstimada): void
    {
        $this->distanciaEstimada = $distanciaEstimada;
    }

}
