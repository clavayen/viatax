<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="vd_viajes_estados_panico")
 * @ORM\Entity(repositoryClass="App\Repository\VdViajesEstadosPanicoRepository")
 */
class VdViajesEstadosPanico
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VdViajes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $viaje;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VdEstadosPanico")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estadoPanico;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $latitud;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $longitud;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaCreacion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViaje(): ?VdViajes
    {
        return $this->viaje;
    }

    public function setViaje(?VdViajes $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }

    public function getEstadoPanico(): ?VdEstadosPanico
    {
        return $this->estadoPanico;
    }

    public function setEstadoPanico(?VdEstadosPanico $estadoPanico): self
    {
        $this->estadoPanico = $estadoPanico;

        return $this;
    }

    public function getLatitud(): ?string
    {
        return $this->latitud;
    }

    public function setLatitud(string $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?string
    {
        return $this->longitud;
    }

    public function setLongitud(string $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }
}
