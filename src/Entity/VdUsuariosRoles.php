<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdUsuariosRoles
 *
 * @ORM\Table(name="vd_usuarios_roles")
 * @ORM\Entity
 */
class VdUsuariosRoles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \VdRoles
     *
     * @ORM\ManyToOne(targetEntity="VdRoles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     * })
     */
    private $rol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuario(): ?VdUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?VdUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getRol(): ?VdRoles
    {
        return $this->rol;
    }

    public function setRol(?VdRoles $rol): self
    {
        $this->rol = $rol;

        return $this;
    }


}
