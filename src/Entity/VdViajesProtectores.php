<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 20/11/18
 * Time: 02:18
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * VdViajesProtectores
 *
 * @ORM\Table(name="vd_viajes_protectores")
 * @ORM\Entity(repositoryClass="App\Repository\TravelProtectorRepository")
 */
class VdViajesProtectores {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \VdViajes
     *
     * @ORM\ManyToOne(targetEntity="VdViajes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viaje_id", referencedColumnName="id")
     * })
     */
    private $viaje;

    /**
     * @var \VdUsuarios
     *
     * @ORM\ManyToOne(targetEntity="VdUsuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="protector_id", referencedColumnName="id")
     * })
     */
    private $protector;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="aceptado", type="boolean", nullable=true)
     */
    private $aceptado = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_aceptacion", type="datetime", nullable=true)
     */
    private $fechaAceptacion;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getViaje(): ?VdViajes
    {
        return $this->viaje;
    }

    public function setViaje(?VdViajes $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }

    public function getProtector(): ?VdUsuarios
    {
        return $this->protector;
    }

    public function setProtector(?VdUsuarios $protector): self
    {
        $this->protector = $protector;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAceptado(): ?bool
    {
        return $this->aceptado;
    }

    /**
     * @param bool|null $aceptado
     */
    public function setAceptado(?bool $aceptado): void
    {
        $this->aceptado = $aceptado;
    }

    /**
     * @return \DateTime
     */
    public function getFechaAceptacion(): \DateTime
    {
        return $this->fechaAceptacion;
    }

    /**
     * @param \DateTime $fechaAceptacion
     */
    public function setFechaAceptacion(\DateTime $fechaAceptacion): void
    {
        $this->fechaAceptacion = $fechaAceptacion;
    }



}