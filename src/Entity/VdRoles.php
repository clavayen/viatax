<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdRoles
 *
 * @ORM\Table(name="vd_roles")
 * @ORM\Entity
 */
class VdRoles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=45, nullable=false)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre_alternativo", type="string", length=45, nullable=true)
     */
    private $nombreAlternativo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getNombreAlternativo(): ?string
    {
        return $this->nombreAlternativo;
    }

    public function setNombreAlternativo(?string $nombreAlternativo): self
    {
        $this->nombreAlternativo = $nombreAlternativo;

        return $this;
    }


}
