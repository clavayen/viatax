<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VdViajesHistorial
 *
 * @ORM\Table(name="vd_viajes_historial")
 * @ORM\Entity(repositoryClass="App\Repository\TravelHistoryRepository")
 */
class VdViajesHistorial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \VdViajes
     *
     * @ORM\ManyToOne(targetEntity="VdViajes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viaje_id", referencedColumnName="id")
     * })
     */
    private $viaje;

    /**
     * @var string
     *
     * @ORM\Column(name="latitud", type="string", length=45, nullable=false)
     */
    private $latitud;

    /**
     * @var string
     *
     * @ORM\Column(name="longitud", type="string", length=45, nullable=false)
     */
    private $longitud;

    /**
     * @var string
     *
     * @ORM\Column(name="canal_socket", type="string", length=45, nullable=false)
     */
    private $canalSocket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fecha = 'CURRENT_TIMESTAMP';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViaje(): ?VdViajes
    {
        return $this->viaje;
    }

    public function setViaje(?VdViajes $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }

    public function getLatitud(): ?string
    {
        return $this->latitud;
    }

    public function setLatitud(string $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?string
    {
        return $this->longitud;
    }

    public function setLongitud(string $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getCanalSocket(): ?string
    {
        return $this->canalSocket;
    }

    public function setCanalSocket(string $canalSocket): self
    {
        $this->canalSocket = $canalSocket;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }


}
