<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Handler;

use App\Entity\VdRoles;
use App\Entity\VdUsuarios;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Description of UsuarioHandler
 *
 * @author coco
 */
class UserHandler extends BaseHandler {

    private $container;
    private $entityManager;
    private $repository;
//    private $url_fullstack;
//    private $parameters;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(VdUsuarios::class);
//        $this->url_fullstack = $url_fullstack;
//        $this->parameters = $parameters;
    }

    public function login($params){
        return $this->repository->login($params);
    }
    
    public function search($first, $max, $sortField, $sortDirection, $searchParam){
        return $this->repository->search($first, $max, $sortField, $sortDirection, $searchParam);
    }
    
    public function get($id){
        return $this->repository->get($id);
    }

    public function save($params){
        extract($params);

        if (!isset($id) || is_null($id)) {
            $user = new VdUsuarios();
            $user->setFechaCreacion(new \DateTime());
        } else {
            $user = $this->entityManager->getReference(VdUsuarios::class,$id);
        }

        if(isset($token)){
            $user->setToken($token);
        }
        $user->setUsuario($username);
        $user->setPassword($password);
        $user->setNombre($name);
        $user->setApellido($lastname);
        $user->setEmail($email);
        $user->setTelefono($phone);
        $user->setDireccion($address);
//        $rol = $this->entityManager->getRepository(VdRoles::class)->findOneById(1);
//        $rol = $this->entityManager->getReference(VdRoles::class,1);
        return $this->repository->save($user);
    }

    public function searchDynamic($param){
        return $this->repository->searchDynamic($param);
    }

}
