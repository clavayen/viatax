<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Handler;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

/**
 * Description of BaseHandler
 *
 * @author coco
 */
class BaseHandler {

    public function toarray($object, $group = null) {
        $this->serializer = SerializerBuilder::create()->build();
        $serializerContext = SerializationContext::create();
        if (!is_null($group)) {
            $serializerContext->setGroups(array($group));
        }
        $serializerContext->setSerializeNull(true);
        return $this->serializer->toArray($object, $serializerContext);
    }
}
