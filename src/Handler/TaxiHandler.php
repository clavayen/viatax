<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 08/11/18
 * Time: 11:57
 */

namespace App\Handler;


use App\Entity\VdCalificaciones;
use App\Entity\VdUsuariosTaxis;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\VdTaxis;

class TaxiHandler extends BaseHandler {

    private $container;
    private $entityManager;
    private $repository;

    /**
     * TaxiHandler constructor.
     * @param $container
     * @param $entityManager
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $this->container = $container;
        $this->repository = $entityManager->getRepository(VdTaxis::class);
        $this->entityManager = $entityManager;
    }

    public function search($first, $max, $sortField, $sortDirection, $searchParam){
        return $this->repository->search($first, $max, $sortField, $sortDirection, $searchParam);
    }

    public function get($id){
        $taxi = $this->repository->get($id);
        $qualification = $this->entityManager->getRepository(VdCalificaciones::class)->getQualificationByTaxi($id);
        $taxi->setQualification($qualification);
        return $taxi;
    }

    public function save($params){
        extract($params);
        if (!isset($id) || is_null($id)) {
            $taxiEntity = new VdTaxis();
            $taxiEntity->setFechaCreacion(new \DateTime());
        } else {
            $taxiEntity = $this->entityManager->getReference(VdTaxis::class,$id);
        }

        $taxiEntity->setPatente($patent);
        $taxiEntity->setModelo($model);
        $taxiEntity->setMarca($trademark);
        $taxiEntity->setEmpresa($company);
        $taxiEntity->setAnio($year);
        $taxiEntity->setDescripcion($description);
        $taxiEntity->setTag($tag);
        return $this->repository->save($taxiEntity);
    }

    public function getResume($id){
        $user_taxi = $this->entityManager->getRepository(VdUsuariosTaxis::class)->getByTaxi($id);
        $qualification = $this->entityManager->getRepository(VdCalificaciones::class)->getQualificationByTaxi($id);
        $user_taxi->getTaxi()->setQualification($qualification);
        return $user_taxi;
    }

}