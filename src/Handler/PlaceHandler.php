<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/10/18
 * Time: 18:29
 */

namespace App\Handler;


use App\Entity\VdLugaresFrecuentes;
use App\Entity\VdUsuarios;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PlaceHandler extends BaseHandler {

    private $container;
    private $entityManager;
    private $repository;

    /**
     * FrequentPlaceHandler constructor.
     * @param $container
     * @param $entityManager
     * @param $repository
     **/
    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager) {
        $this->container = $container;
        $this->repository = $entityManager->getRepository(VdLugaresFrecuentes::class);
        $this->entityManager = $entityManager;
    }

    public function search($first, $max, $sortField, $sortDirection, $searchParam){
        return $this->repository->search($first, $max, $sortField, $sortDirection, $searchParam);
    }

    public function get($id){
        return $this->repository->get($id);
    }

    public function save($params){
        $entity = new VdLugaresFrecuentes();
        $user = $this->entityManager->getReference(VdUsuarios::class,$params["userId"]);
        $entity->setFechaCreacion(new \DateTime());
        $entity->setUsuario($user);
        $entity = $this->setData($entity,$params);
        return $this->repository->save($entity);
    }

    public function put($params){
        $entity = $this->entityManager->getReference(VdLugaresFrecuentes::class,$params["id"]);
        $entity = $this->setData($entity,$params);
        return $this->repository->save($entity);
    }

    public function delete($id){
        $entity = $this->entityManager->getReference(VdLugaresFrecuentes::class,$id);
        return $this->repository->delete($entity);
    }

    private function setData(VdLugaresFrecuentes $entity, $data){
        extract($data);
        $entity->setDescripcion($description);
        $entity->setDireccion($address);
        $entity->setNumero($number);
        $entity->setLocalidad($location);
        $entity->setLatitud($latitude);
        $entity->setLongitud($longitude);
        return $entity;
    }

    public function getAllByUser($id){
        return $this->repository->getAllByUser($id);
    }


}