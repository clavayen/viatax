<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/10/18
 * Time: 17:38
 */

namespace App\Handler;

use App\Entity\VdUsuarios;
use App\Entity\VdUsuariosProtectores;
use App\Utils\Constants;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ProtectorHandler extends BaseHandler {

    private $container;
    private $entityManager;
    private $repository;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(VdUsuariosProtectores::class);
    }

    public function search($first, $max, $sortField, $sortDirection, $searchParam){
        return $this->repository->search($first, $max, $sortField, $sortDirection, $searchParam);
    }

    public function searchRequest($first, $max, $sortField, $sortDirection, $searchParam){
        return $this->repository->searchRequest($first, $max, $sortField, $sortDirection, $searchParam);
    }

    public function searchMyProtected($first, $max, $sortField, $sortDirection, $searchParam){
        return $this->repository->searchMyProtected($first, $max, $sortField, $sortDirection, $searchParam);
    }

    public function get($id){
        return $this->repository->get($id);
    }

    public function save($params){
        extract($params);
        $entity = new VdUsuariosProtectores();
        $user = $this->entityManager->getReference(VdUsuarios::class,$userId);
        $userProtector = $this->entityManager->getReference(VdUsuarios::class,$protectorId);
        $entity->setUsuario($user);
        $entity->setProtector($userProtector);
        $entity->setRelacion($relation);
        $entity->setFechaCreacion(new \DateTime());
        $entity->setEstado(Constants::PROTECTOR_PENDING);
        return $this->repository->save($entity);
    }

    public function delete($id){
        $entity = $this->entityManager->getReference(VdUsuariosProtectores::class,$id);
        return $this->repository->rejectRequest($entity);
    }

    public function confirmRequest($id){
        $entity = $this->entityManager->getReference(VdUsuariosProtectores::class,$id);
        $entity->setEstado(Constants::PROTECTOR_ADDED);
        return $this->repository->confirmRequest($entity);
    }

    public function rejectRequest($id){
        $entity = $this->entityManager->getReference(VdUsuariosProtectores::class,$id);
        return $this->repository->rejectRequest($entity);
    }

}