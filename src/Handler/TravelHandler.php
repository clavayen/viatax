<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 19/10/18
 * Time: 21:34
 */

namespace App\Handler;

use App\Entity\VdCalificaciones;
use App\Entity\VdEstadosPanico;
use App\Entity\VdEstadosViajes;
use App\Entity\VdLugaresFrecuentes;
use App\Entity\VdTaxis;
use App\Entity\VdViajes;
use App\Entity\VdUsuarios;
/*Aws Amazon*/

use App\Entity\VdViajesEstadosPanico;
use App\Entity\VdViajesHistorial;
use App\Entity\VdViajesProtectores;
use App\Utils\Constants;
use Aws\S3\S3Client;
use Aws\Ec2\Ec2Client;
use Aws\Exception\AwsException;
use Aws\Sns\SnsClient;
/*socket*/

use Doctrine\ORM\EntityManagerInterface;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TravelHandler extends BaseHandler {

    private $container;
    private $entityManager;
    private $repository;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(VdViajes::class);
    }

    public function search($first, $max, $sortField, $sortDirection, $searchParam){
        $travels = $this->repository->search($first, $max, $sortField, $sortDirection, $searchParam);
//        for ($i = 0; $i < count($travels); $i++){
//            $travels[$i]['coordenadas'] = $this->entityManager->getRepository(VdViajesHistorial::class)->getById($travels['id']);
//        }
        return $travels;
    }

    public function get($id){
        $data = $this->repository->get($id);
        if(!is_null($data)){
            $coordinates = $this->entityManager->getRepository(VdViajesHistorial::class)->getByTravelId($data->getId());
            $data->setCoordenadas($coordinates);
        }
        return $data;
    }

    public function save($params){
        extract($params);
        $place = null;
        if(isset($params["taxiId"]) && !is_null($params["taxiId"])){
            $taxiId = $params["taxiId"];
        }else{
            $taxiId = 1;
        }

        if(isset($placeId) && !is_null($placeId)){
            $place = $this->entityManager->getReference(VdLugaresFrecuentes::class,$placeId);
        }
        $stateTravel = $this->entityManager->getRepository(VdEstadosViajes::class)->findOneByNombre(Constants::STATE_TRAVEL_INITIATED);
        $user = $this->entityManager->getReference(VdUsuarios::class,$userId);
        $taxi = $this->entityManager->getReference(VdTaxis::class,$taxiId);
        $nroViaje = time().rand(1, 1000000);
        $channelSocket = $nroViaje."_".$userId."_".$taxiId;

        $entity = new VdViajes();
        $entity->setNroViaje($nroViaje);
        $entity->setDescripcion($description);
        $entity->setFechaCreacion(new \DateTime());
        $entity->setLatitudInicio($latIni);
        $entity->setLongitudInicio($lngIni);
        $entity->setLatitudFin($latFin);
        $entity->setLongitudFin($lngFin);
        $entity->setUsuario($user);
        $entity->setLugarFrecuente($place);
        $entity->setTaxi($taxi);
        $entity->setCanalSocket($channelSocket);
        $entity->setEstadoViaje($stateTravel);

        if (isset($totalTime)){
            $entity->setTiempoEstimado($totalTime);
        }
        if (isset($totalDistance)){
            $entity->setDistanciaEstimada($totalDistance);
        }
        if (isset($price)){
            $entity->setPrecio($price);
        }

        $this->repository->save($entity,$protectorsId);
        $this->createChannel($user,$channelSocket);
//        $this->sendMessageProtector($protectorsId);
    }

    private function createChannel($user,$channelSocket){
        $version = new Version2X("//127.0.0.1:8017");
        $client = new Client($version);
        $userName = "user_".$user->getUsuario();
        $client->initialize();
        $client->emit('createChannel', array('userId' => $user->getId(),'userName' => $userName, 'channelId' => $channelSocket));
        $client->close();
    }

    private function sendMessageProtector($protectorsArray){
        $numbersArray = array("+54 388 513 0544","+54 388 571 5113");
        $options = array(
            'region' => 'us-east-1',
            'version' => 'latest',
            'profile' => 'coco-user',
        );
        $sns = new SnsClient($options);
        $args = array(
            "SenderID" => "Viatax",
            "SMSType" => "Promotional",
            "Message" => "Bruno Lavayen ha solicitado tu seguimiento en TAGXI: www.link.aplicacion.com",
            "PhoneNumber" => "+54 388 513 0544",
//            "PhoneNumber" => "+54 388 590 1068"
        );
        foreach ($numbersArray as $number) {
            $phoneNumber = $number;
            $args["PhoneNumber"] = $phoneNumber;
            $result = $sns->publish($args);
        }
        return "Mensajes Enviados Corectamente";
    }

    public function getPendingByUser($params){
        $userId = (isset($params["userId"])?$params["userId"]:1);
        $data = $this->repository->getPendingByUser($userId);
        if(!is_null($data)){
            $data['coordenadas'] = $this->entityManager->getRepository(VdViajesHistorial::class)->getByTravelId($data['id']);
        }
        return $data;
    }

    public function saveDetail($params){
        extract($params);
        $travel = $this->entityManager->getReference(VdViajes::class,$travelId);

        $entity = new VdViajesHistorial();
        $entity->setViaje($travel);
        $entity->setCanalSocket($travel->getCanalSocket());
        $entity->setLatitud($lat);
        $entity->setLongitud($lng);
        $entity->setFecha(new \DateTime());
        $this->repository->saveDetail($entity);
        $this->sendCoordinatesToSocket($travel->getCanalSocket(),$entity);
    }

    private function sendCoordinatesToSocket($channelSocket,$positionActual){
        $data = array(
            "channelId" => $channelSocket,
            "lat" => $positionActual->getLatitud(),
            "lng" => $positionActual->getLongitud(),
        );

        $version = new Version2X("//127.0.0.1:8017");
        $client = new Client($version);
        $client->initialize();
        $client->emit('send-coordinates', $data);
        $client->close();
    }

    public function endTravel($params){
        extract($params);
        $entity = $this->entityManager->getReference(VdViajes::class,$travelId);
        $stateTravel = $this->entityManager->getRepository(VdEstadosViajes::class)->findOneByNombre(Constants::STATE_TRAVEL_FINALIZED);
        $entity->setPrecio(isset($price) ? $price : '');
        $entity->setDistanciaEstimada(isset($distance) ? $distance : '');
        $entity->setTiempoEstimado(isset($timeEstimated)?$timeEstimated:'');
        $entity->setEstadoViaje($stateTravel);
        $entity->setFechaFinalizacion(new \DateTime());
        $entity_qualification = $this->getCalification($params);
        $entity_qualification->setViaje($entity);
        $this->repository->endTravelAndQualify($entity,$entity_qualification);
        $this->closeChannel($entity);
        return $params;
    }

    private function getCalification($params){
        $travel_qualification = $this->entityManager->getRepository(VdCalificaciones::class)->findOneByViaje($params["travelId"]);
        if(is_null($travel_qualification)){
            $travel_qualification = new VdCalificaciones();
        }

        if(isset($params["qualification"]) && !is_null($params["qualification"])){
            $qualification = $params["qualification"];
            $cortesy = $qualification["cortesy"];
            $route = $qualification["routeSelected"];
            $carState = $qualification["carState"];
            $driving = $qualification["driving"];
            $total = ((double)$cortesy + (double)$route +(double)$carState +(double)$driving) / 4;
        }else{
            $cortesy = Constants::VALUE_CORTESY_DEFAULT;
            $route = Constants::VALUE_ROUTE_DEFAULT;
            $carState = Constants::VALUE_CAR_STATE_DEFAULT;
            $driving = Constants::VALUE_DRIVING_DEFAULT;
            $total = Constants::VALUE_TOTAL_DEFAULT;
        }

        $travel_qualification->setCortesia($cortesy);
        $travel_qualification->setRutaSeleccionada($route);
        $travel_qualification->setEstadoVehiculo($carState);
        $travel_qualification->setConduccion($driving);
        $travel_qualification->setTotal($total);
        return $travel_qualification;
    }

    public function openTravel($params){
        extract($params);
        $entity = $this->entityManager->getReference(VdViajes::class,$travelId);
        $stateTravel = $this->entityManager->getRepository(VdEstadosViajes::class)->findOneByNombre(Constants::STATE_TRAVEL_INITIATED);
        $entity->setEstadoViaje($stateTravel);
        $entity->setFechaFinalizacion(new \DateTime());
        $this->repository->update($entity);
        $this->openChannel($entity);
    }

    public function searchRequestByProtector($first, $max, $sortField, $sortDirection, $searchParam){
        $accepted = false;
        $state = Constants::STATE_TRAVEL_INITIATED;
        return $this->entityManager->getRepository(VdViajesProtectores::class)->searchByProtector($first, $max, $sortField, $sortDirection, $searchParam, $accepted, $state);
    }

    public function searchAcceptedByProtector($first, $max, $sortField, $sortDirection, $searchParam){
        $accepted = true;
        $state = null;
        return $this->entityManager->getRepository(VdViajesProtectores::class)->searchByProtector($first, $max, $sortField, $sortDirection, $searchParam, $accepted, $state);
    }

    public function acceptedOrReject($params){
        $entity = $this->entityManager->getReference(VdViajesProtectores::class,$params["travelProtectorId"]);
        $entity->setAceptado($params["accepted"]);
        $entity->setFechaAceptacion(new \DateTime());
        $this->entityManager->getRepository(VdViajesProtectores::class)->save($entity);
    }

    public function openChannelsSocket(){
//        return $this->repository->searchTravelsOpen();
        $travels = $this->repository->searchTravelsOpen();
        $dataSendSocket = array();
        $version = new Version2X("//127.0.0.1:8017");
        $client = new Client($version);

        foreach ($travels as $travel) {
            $user = $travel["usuario"];
            $taxi = $travel["taxi"];
            $channelSocket = $travel["nroViaje"]."_".$user["id"]."_".$taxi["id"];
            $userName = "user_".$user["usuario"];
            $data = array(
                "userId" => $user["id"],
                "userName" => $userName,
                "channelId" => $channelSocket
            );
            array_push($dataSendSocket,$data);
        }

        $client->initialize();
        $client->emit('listChannel', $dataSendSocket);
        $client->close();
    }

    private function closeChannel(VdViajes $travel){
        $channelSocket = $travel->getNroViaje()."_".$travel->getUsuario()->getId()."_".$travel->getTaxi()->getId();
        $version = new Version2X("//127.0.0.1:8017");
        $client = new Client($version);
        $client->initialize();
        $client->emit('closeChannel', array("channelId" => $channelSocket));
        $client->close();
    }

    private function openChannel(VdViajes $travel){
        $userId = $travel->getUsuario()->getId();
        $userName = "user_".$travel->getUsuario()->getUsuario();
        $channelSocket = $travel->getNroViaje()."_".$travel->getUsuario()->getId()."_".$travel->getTaxi()->getId();
        $version = new Version2X("//127.0.0.1:8017");
        $client = new Client($version);
        $client->initialize();
        $client->emit('openChannel', array('userId' => $userId,'userName' => $userName, 'channelId' => $channelSocket));
        $client->close();
    }

    private function getPanicState($state){
        if(strtoupper($state) === Constants::PANIC_STATE_HAPPY){
            $panicState = Constants::PANIC_STATE_HAPPY;
        }elseif (strtoupper($state) === Constants::PANIC_STATE_WARNING){
            $panicState = Constants::PANIC_STATE_WARNING;
        }elseif (strtoupper($state) === Constants::PANIC_STATE_DANGER){
            $panicState = Constants::PANIC_STATE_DANGER;
        }else{
            $panicState = Constants::PANIC_STATE_HAPPY;
        }
        return $this->entityManager->getRepository(VdEstadosPanico::class)->findOneByNombre($panicState);
    }

    private function sendPanicStateToSocket($channelSocket,$travelPanicState){
        $data = array(
            "channelId" => $channelSocket,
            "lat" => $travelPanicState->getLatitud(),
            "lng" => $travelPanicState->getLongitud(),
            "panicStateName" => $travelPanicState->getEstadoPanico()->getNombre(),
            "panicStateDescription" => $travelPanicState->getEstadoPanico()->getDescripcion(),
        );

        $version = new Version2X("//127.0.0.1:8017");
        $client = new Client($version);
        $client->initialize();
        $client->emit('send-panic-state', $data);
        $client->close();
    }

    public function sendPanicState($params){
        extract($params);
        $travel = $this->entityManager->getReference(VdViajes::class,$params["travelId"]);
        $panicState = $this->getPanicState($params["panicState"]);
        $entity = new VdViajesEstadosPanico();
        $entity->setViaje($travel);
        $entity->setEstadoPanico($panicState);
        $entity->setLatitud($params["latitude"]);
        $entity->setLongitud($params["longitude"]);
        $entity->setFechaCreacion(new \DateTime());
        $this->entityManager->getRepository(VdViajesEstadosPanico::class)->save($entity);
        $this->sendPanicStateToSocket($travel->getCanalSocket(),$entity);
    }

    public function getPanicStateByTravel($travelId){
        return $this->entityManager->getRepository(VdViajesEstadosPanico::class)->getPanicStateByTravel($travelId);
    }

}
