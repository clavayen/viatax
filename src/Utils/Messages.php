<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 09/11/18
 * Time: 08:54
 */

namespace App\Utils;


class Messages
{
    const OK = "Exito";
    const FAILED = "Ocurrio un error";
    const CREATED = "Registro creado exitosamente";
    const EDITED = "Registro actualizado exitosamente";
    const SAVE_COORDINATES = "Ok coordenadas guardadas correctamente";
    const END_TRAVEL = "Ok Viaje finalizado";
    const OPEN_TRAVEL = "Ok Viaje abierto nuevamente";
    const ACCEPTED_REQUEST = "Solicitud aceptada. Gracias por acompañar a su protegido en este viaje";
    const REJECT_REQUEST = "Has rechazado la solitiud de viaje";
    const SEND_DATA_SOCKET_OK = "Datos enviados al socket correctamente";
}