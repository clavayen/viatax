<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 08/11/18
 * Time: 22:09
 */

namespace App\Utils;


class Constants
{
    const FORMAT_SEARCH = 0;
    const FORMAT_REGISTER = 1;
    const FORMAT_EDIT = 2;
    const FORMAT_VIEW = 3;
    const FORMAT_UNSUBSCRIBE = 4;

    const PROTECTOR_PENDING = 0;
    const PROTECTOR_ADDED = 1;

    const TRAVEL_INITIATED = 1;
    const TRAVEL_FINALIZATED = 2;
    const TRAVEL_PENDING = 3;

    const STATE_TRAVEL_INITIATED = "INICIADO";
    const STATE_TRAVEL_FINALIZED = "FINALIZADO";

    const VALUE_CORTESY_DEFAULT = 3;
    const VALUE_DRIVING_DEFAULT = 3;
    const VALUE_ROUTE_DEFAULT = 3;
    const VALUE_CAR_STATE_DEFAULT = 3;
    const VALUE_TOTAL_DEFAULT = 3;

    const PANIC_STATE_HAPPY = 'STATE_HAPPY';
    const PANIC_STATE_WARNING = 'STATE_WARNING';
    const PANIC_STATE_DANGER = 'STATE_DANGER';
}