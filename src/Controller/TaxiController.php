<?php

namespace App\Controller;

use App\Handler\TaxiHandler;
use App\Utils\Codes;
use App\Utils\Messages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @RouteResource("Taxis",pluralize=false)
 */

class TaxiController extends BaseController {

    /**
     * List of cab.
     *
     * Este metodo devuelve un listado de taxis paginados segun el numero enviado por parametro
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdTaxis::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdTaxis")
     * @Security(name="Bearer")
     */
    public function postSearchAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $userHandler = $this->container->get(TaxiHandler::class);
            $lp = $userHandler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
        $email = $this->container->getParameter('url_fullstack');
    }

    /**
     * taxi by Id.
     *
     * Devuelve un taxi especifico segun el id
     * @SWG\Response(response=Codes::OK,description="Objeto Taxi")
     * @SWG\Tag(name="VdTaxis")
     * @Security(name="Bearer")
     */
    public function getAction($id) {
        try {
            $taxiHandler = $this->container->get(TaxiHandler::class);
            $data = $taxiHandler->get($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Created Taxi.
     *
     * Este metodo da de alta un nuevo taxi
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdTaxis::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdTaxis")
     * @Security(name="Bearer")
     */
    public function postSaveAction(Request $request) {
        try {
            $params = $request->request->all();
            $taxiHandler = $this->container->get(TaxiHandler::class);
            $data = $taxiHandler->save($params);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::CREATED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Updated Taxi.
     *
     * Este metodo actualiza un taxi
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdTaxis::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdTaxis")
     * @Security(name="Bearer")
     */
    public function putAction(Request $request) {
        try {
            $params = $request->request->all();
            $taxiHandler = $this->container->get(TaxiHandler::class);
            $data = $taxiHandler->save($params);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::EDITED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * info taxi resumen by Id.
     *
     * Devuelve el resumen de un taxi segun el id
     * @SWG\Response(response=Codes::OK,description="Objeto Taxi")
     * @SWG\Tag(name="VdTaxis")
     * @Security(name="Bearer")
     */
    public function getResumeAction($id) {
        try {
            $taxiHandler = $this->container->get(TaxiHandler::class);
            $data = $taxiHandler->getResume($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

}
