<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/10/18
 * Time: 18:45
 */

namespace App\Controller;

use App\Utils\Codes;
use App\Utils\Messages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use App\Handler\PlaceHandler;

/**
 * @RouteResource("Places",pluralize=false)
 */

class PlaceController extends BaseController {

    /**
     * List of Frequent Places.
     *
     * Este metodo devuelve un listado de lugares paginados segun el numero enviado por parametro
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdLugaresFrecuentes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdLugaresFrecuentes")
     * @Security(name="Bearer")
     */
    public function postSearchAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $handler = $this->container->get(PlaceHandler::class);
            $lp = $handler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * place by Id.
     *
     * Devuelve un lugar especifico segun el id
     * @SWG\Response(response=200,description="Objeto Usuario")
     * @SWG\Tag(name="VdLugaresFrecuentes")
     * @Security(name="Bearer")
     */
    public function getAction($id) {
        try {
            $handler = $this->container->get(PlaceHandler::class);
            $data = $handler->get($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Created Frequent Place.
     *
     * Este metodo da de alta un nuevo lugar frecuente
     * @SWG\Response(response=200,description="Ok",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdTaxis::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdLugaresFrecuentes")
     * @Security(name="Bearer")
     */
    public function postSaveAction(Request $request) {
        try {
            $params = $request->request->all();
            $handler = $this->container->get(PlaceHandler::class);
            $data = $handler->save($params);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::CREATED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Update Frequent Place.
     *
     * Actualiza un lugar frecuente
     * @SWG\Response(response=200,description="Ok Confirmado")
     * @SWG\Tag(name="VdLugaresFrecuentes")
     * @Security(name="Bearer")
     */
    public function putAction(Request $request) {
        try {
            $params = $request->request->all();
            $handler = $this->container->get(PlaceHandler::class);
            $data = $handler->put($params);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::EDITED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Delete frequent Place.
     *
     * Elimina un lugar frecuente
     * @SWG\Response(response=200,description="Ok Confirmado")
     * @SWG\Tag(name="VdLugaresFrecuentes")
     * @Security(name="Bearer")
     */
    public function deleteAction($id) {
        try {
            $handler = $this->container->get(PlaceHandler::class);
            $data = $handler->delete($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * place by Id.
     *
     * Devuelve un lugar especifico segun el id
     * @SWG\Response(response=200,description="Objeto Usuario")
     * @SWG\Tag(name="VdLugaresFrecuentes")
     * @Security(name="Bearer")
     * @Rest\Get("/places/user/{id}/all")
     */
    public function getAllUserAction($id) {
        try {
            $handler = $this->container->get(PlaceHandler::class);
            $data = $handler->getAllByUser($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

}