<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/10/18
 * Time: 17:31
 */

namespace App\Controller;

use App\Utils\Codes;
use App\Utils\Messages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use App\Handler\ProtectorHandler;

/**
 * @RouteResource("Protectors",pluralize=false)
 */

class ProtectorController extends BaseController
{

    /**
     * List of Protectors.
     *
     * Este metodo devuelve un listado de usuarios protectores (estado 1) paginados segun el numero enviado por parametro
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuariosProtectores::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function postSearchAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $lp = $protectorHandler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * List of Protectors.
     *
     * Este metodo devuelve un listado de solicitudes de proteccion (estado 0) paginados segun el numero enviado por parametro
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuariosProtectores::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function postSearchRequestAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $data = $protectorHandler->searchRequest($offset, $limit, $sortField, $sortDirection, $searchParam);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Created User Protector.
     *
     * Este metodo da de alta un nuevo protector
     * @SWG\Response(response=Codes::OK,description="Creacion de un nuevo usuario protector",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuariosProtectores::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function postSaveAction(Request $request) {
        try {
            $params = $request->request->all();
            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $data = $protectorHandler->save($params);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::CREATED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Delete Protector.
     *
     * Elimina un usuario protector
     * @SWG\Response(response=200,description="Ok Confirmado")
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function deleteAction($id) {
        try {
            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $data = $protectorHandler->delete($id);
            $this->response->setData($data);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * confirm request Protection.
     *
     * Confirma el pedido de proteccion de un usuario
     * @SWG\Response(response=200,description="Ok Confirmado")
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function putRequestAction($id) {
        try {
            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $data = $protectorHandler->confirmRequest($id);
            $this->response->setData($data);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * reject request Protection.
     *
     * Confirma el pedido de proteccion de un usuario
     * @SWG\Response(response=200,description="Ok Confirmado")
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function deleteRequestAction($id) {
        try {
            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $data = $protectorHandler->rejectRequest($id);
            $this->response->setData($data);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * List of My Protecteds.
     *
     * Este metodo devuelve un listado de usuarios a quienes protejo, paginados segun el numero enviado por parametro
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuariosProtectores::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuariosProtectores")
     * @Security(name="Bearer")
     */
    public function postSearchMyprotectedAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $protectorHandler = $this->container->get(ProtectorHandler::class);
            $data = $protectorHandler->searchMyProtected($offset, $limit, $sortField, $sortDirection, $searchParam);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

}