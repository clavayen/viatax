<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 20/11/18
 * Time: 01:46
 */

namespace App\Controller;

use App\Handler\TravelHandler;
use App\Utils\Codes;
use App\Utils\Messages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @RouteResource("Travels",pluralize=false)
 */

class TravelController extends BaseController {

    /**
     * List of cab.
     *
     * Este metodo devuelve un listado de taxis paginados segun el numero enviado por parametro
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postSearchAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $this->setTamanioPagina($this->getTamanioPagina());
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $handler = $this->container->get(TravelHandler::class);
            $lp = $handler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
        $email = $this->container->getParameter('url_fullstack');
    }

    /**
     * taxi by Id.
     *
     * Devuelve un taxi especifico segun el id
     * @SWG\Response(response=Codes::OK,description="Objeto Taxi")
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function getAction($id) {
        try {
            $handler = $this->container->get(TravelHandler::class);
            $data = $handler->get($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Created un nuevo viaje.
     *
     * Este metodo da de alta un nuevo taxi
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postSaveAction(Request $request) {
        try {
            $params = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $handler->save($params);
            $this->response->setCode(Codes::CREATED);
            $this->response->setMessage(Messages::CREATED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * travel pending .
     *
     * Devuelve un viaje pendiente de cierre (debe haber uno solo siempre)
     * @SWG\Response(response=Codes::OK,description="Objeto Viaje")
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postPendingCloseAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $data = $handler->getPendingByUser($searchParam);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Created Travel.
     *
     * Este metodo da de alta un nuevo taxi
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postDetailSaveAction(Request $request) {
        try {
            $params = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $data = $handler->saveDetail($params);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::SAVE_COORDINATES);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Finaliza un viaje.
     *
     * Este metodo Cambia el estado del viaje a finalizado
     * @SWG\Response(response=Codes::OK,description="Confirmacion de Fin del Viaje",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postEndTravelAction(Request $request) {
        try {
            $params = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $data = $handler->endTravel($params);
            $this->response->setCode(Codes::OK);
            $this->response->setData($data);
            $this->response->setMessage(Messages::END_TRAVEL);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Finaliza un viaje.
     *
     * Este metodo Cambia el estado del viaje a finalizado
     * @SWG\Response(response=Codes::OK,description="Confirmacion de Fin del Viaje",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postOpenTravelAction(Request $request) {
        try {
            $params = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $handler->openTravel($params);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OPEN_TRAVEL);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * List of cab.
     *
     * Este metodo devuelve un listado de taxis paginados segun el numero enviado por parametro
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postProtectorRequestAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $handler = $this->container->get(TravelHandler::class);
            $lp = $handler->searchRequestByProtector($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
        $email = $this->container->getParameter('url_fullstack');
    }

    /**
     * List of cab.
     *
     * Este metodo devuelve un listado de taxis paginados segun el numero enviado por parametro
     * @SWG\Response(response=Codes::OK,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdViajes::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postProtectorAcceptedAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $this->setTamanioPagina($this->getTamanioPagina());
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $handler = $this->container->get(TravelHandler::class);
            $lp = $handler->searchAcceptedByProtector($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
        $email = $this->container->getParameter('url_fullstack');
    }

    /**
     * travel pending .
     *
     * Devuelve un viaje pendiente de cierre (debe haber uno solo siempre)
     * @SWG\Response(response=Codes::OK,description="Objeto Viaje")
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postAcceptedAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $data = $handler->acceptedOrReject($searchParam);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            if($searchParam["accepted"] == 1 || $searchParam["accepted"] == true){
                $message = Messages::ACCEPTED_REQUEST;
            }else{
                $message = Messages::REJECT_REQUEST;
            }
            $this->response->setMessage($message);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * travel pending .
     *
     * Devuelve un viaje pendiente de cierre (debe haber uno solo siempre)
     * @SWG\Response(response=Codes::OK,description="Objeto Viaje")
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postOpenChannelsAction() {
        try {
            $handler = $this->container->get(TravelHandler::class);
            $handler->openChannelsSocket();
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::SEND_DATA_SOCKET_OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * Panic State .
     *
     * Avisa a traves del socket, el estado de panico de un usuario.
     * @SWG\Response(response=Codes::OK,description="Objeto Viaje")
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function postPanicStateAction(Request $request) {
        try {
            $data = $request->request->all();
            $handler = $this->container->get(TravelHandler::class);
            $handler->sendPanicState($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::SEND_DATA_SOCKET_OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * travel last panic state.
     *
     * Devuelve el ultimo estado de panicodel usuario (puede haber 0 o mas)
     * @SWG\Response(response=Codes::OK,description="Objeto Viaje")
     * @SWG\Tag(name="VdViajes")
     * @Security(name="Bearer")
     */
    public function getPanicStateAction($id) {
        try {
            $handler = $this->container->get(TravelHandler::class);
            $data = $handler->getPanicStateByTravel($id);
            $this->response->setData($data);
            $this->response->setCode(Codes::OK);
            $this->response->setMessage(Messages::OK);
        } catch (HttpException $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }
}