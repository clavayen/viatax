<?php

namespace App\Controller;

use App\Utils\Messages;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use App\Handler\UserHandler;

/**
 * @RouteResource("Users",pluralize=false)
 */

class UserController extends BaseController
{

    /**
     * Login from users.
     *
     * Este metodo devuelve valida el acceso a la aplicacion a traves de las credenciales
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuarios::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuarios")
     */
    public function postLoginAction(Request $request) {
        try {
            $params = $request->request->all();
            $userHandler = $this->container->get(UserHandler::class);
            $data = $userHandler->login($params);
            $this->response->setData($data);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }
    
     /**
     * List of users.
     *
     * Este metodo devuelve un listado de usuarios paginados segun el numero enviado por parametro
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="sortField",in="query",type="string",description="Campo utilizado para ordenar")
     * @SWG\Parameter(name="sortDirection",in="query",type="string",description="Tipo de ordenacion")
     * @SWG\Parameter(name="currentPage",in="query",type="integer",description="Pagina actual")
     * @SWG\Parameter(name="filtros",in="body",
      *     @Model(type=VdUsuarios::class,groups={"dto"})
      * )
     * @SWG\Tag(name="VdUsuarios")
     * @Security(name="Bearer")
     */
    public function postSearchAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $sortField = $request->query->get("sortField");
            $sortDirection = $request->query->get('sortDirection');
            $currentPage = $request->query->get('currentPage');
            $currentPage = null == $currentPage ? 1 : $currentPage;
            $offset =  ($currentPage-1) * $this->getTamanioPagina();
            $limit = $this->getTamanioPagina() ;

            $userHandler = $this->container->get(UserHandler::class);
            $lp = $userHandler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize( $this->getTamanioPagina() );
            $this->response->setData($lp);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
        $email = $this->container->getParameter('url_fullstack');
    }
    
    /**
     * user by Id.
     *
     * Devuelve un usuario especifico segun el id
     * @SWG\Response(response=200,description="Objeto Usuario")
     * @SWG\Tag(name="VdUsuarios")
     * @Security(name="Bearer")
     */
    public function getAction($id) {
        try {
            $userHandler = $this->container->get(UserHandler::class);
            $data = $userHandler->get($id);
            $this->response->setData($data);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::OK);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * List of users.
     *
     * Este metodo crea un nuevo usuario
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuarios::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuarios")
     * @Security(name="Bearer")
     */
    public function postSaveAction(Request $request) {
        try {
            $params = $request->request->all();
            $userHandler = $this->container->get(UserHandler::class);
            $data = $userHandler->save($params);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::CREATED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }

    /**
     * List of users.
     *
     * Este metodo actualiza un usuario
     * @SWG\Response(response=200,description="Arreglo de Usuarios",)
     * @SWG\Parameter(name="filtros",in="body",
     *     @Model(type=VdUsuarios::class,groups={"dto"})
     * )
     * @SWG\Tag(name="VdUsuarios")
     * @Security(name="Bearer")
     */
    public function putAction(Request $request) {
        try {
            $params = $request->request->all();
            $userHandler = $this->container->get(UserHandler::class);
            $data = $userHandler->save($params);
            $this->response->setCode(200);
            $this->response->setMessage(Messages::EDITED);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $this->response;
    }
    
    /*esta es otra manera de llamar a un controlador, se pinta toda la ruta*/
    /**
    * @Rest\Post("api/usuario", name="api_name", requirements={"_format"="json|xml"})
    */
    public function getNuevaSolicitud(){
        
    }

    /**
     * Search of users.
     *
     * Este metodo devuelve un listado de usuarios que coninciden con la cadena ingresada
     */
    public function postSearchDynamicAction(Request $request) {
        try {
            $searchParam = $request->request->all();
            $userHandler = $this->container->get(UserHandler::class);
            $data = $userHandler->searchDynamic($searchParam["q"]);
        } catch (\Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        return $data;
    }
    
}
