<?php

namespace App\Controller;

use App\Dto\ResponseDto;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Description of BaseController
 *
 * @author coco
 */
class BaseController extends FOSRestController {

    protected $response;
    private $tamanioPagina = 10;

    function __construct(ResponseDto $response) {
        $this->response = $response;
        $this->response->setData(null);
        $this->response->setCode(200);
        $this->response->setMessage("");
    }
    
    public function  getTamanioPagina(){
        return $this->tamanioPagina;	
    }

    public function  setTamanioPagina($tamanio){
        $this->tamanioPagina = $tamanio;
        return $this;
    }

}