<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 20/11/18
 * Time: 02:07
 */

namespace App\Repository;

use App\Entity\VdCalificaciones;
use App\Entity\VdViajes;
use App\Entity\VdViajesHistorial;
use App\Entity\VdViajesProtectores;
use Doctrine\ORM\EntityRepository;
use App\Entity\VdUsuarios;
use App\Pagination\ListadoPaginar;
use App\Utils\Constants;
use Doctrine\ORM\Query;

class TravelRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder('t')
                    ->innerJoin("t.usuario","u")
                    ->innerJoin("t.taxi","ta")
                    ->innerJoin("t.estadoViaje", "e")
                    ->leftJoin("t.calificacionViaje", "c");

        if (!empty($userId)) {
            $qb->andWhere($qb->expr()->eq('u.id', $userId ));
        }

        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id')) ? $sortField : 'id';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('t.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(t)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('t,u,ta,e,c')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function get($id) {
        try {
            $qb = $this->createQueryBuilder('u');
            $qb->where($qb->expr()->eq('u.id', $id ));
            $result = $qb->select('u')->getQuery()->getOneOrNullResult();
            return $result;
        } catch (\Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }

    public function save(VdViajes $entity, $protectorsId) {
        $em = $this->getEntityManager();
        try {

            $em->getConnection()->beginTransaction();
            foreach ($protectorsId as $userId) {
//                $protector = $this->getEntityManager()->getRepository(VdUsuarios::class)->findOneById($userId);
                $protector = $em->getReference(VdUsuarios::class,$userId);
                $entity_protector = new VdViajesProtectores();
                $entity_protector->setViaje($entity);
                $entity_protector->setProtector($protector);
                $em->persist($entity_protector);
            }

            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

    public function getPendingByUser($userId) {
        $qb = $this->createQueryBuilder('t')
            ->innerJoin("t.usuario","u")
            ->innerJoin("t.estadoViaje","e");

        $qb->where($qb->expr()->eq('u.id',$userId ))
            ->andWhere($qb->expr()->eq('e.nombre',"'".Constants::STATE_TRAVEL_INITIATED."'" ))
            ->orderBy('t.id', "DESC")
            ->setMaxResults(1);

        $results = $qb->select('t,u,e')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        return (count($results)===1)?$results[0]:null;
    }

    public function saveDetail(VdViajesHistorial $entity) {
        $em = $this->getEntityManager();
        try {
            $em->getConnection()->beginTransaction();
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

    public function update(VdViajes $entity) {
        $em = $this->getEntityManager();
        try {
            $em->getConnection()->beginTransaction();
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

    function searchTravelsOpen(){
        $qb = $this->createQueryBuilder('t')
            ->innerJoin("t.usuario","u")
            ->innerJoin("t.taxi","ta")
            ->innerJoin("t.estadoViaje", "e")
            ->where("e.nombre = :pEstadoViaje")
            ->setParameter("pEstadoViaje", Constants::STATE_TRAVEL_INITIATED);

        return  $qb->select('t,ta,u')->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function endTravelAndQualify(VdViajes $entity, VdCalificaciones $entity_qualification) {
        $em = $this->getEntityManager();
        try {
            $em->getConnection()->beginTransaction();
            $em->persist($entity);
            $em->persist($entity_qualification);
            $em->flush();
            $em->getConnection()->commit();
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

}