<?php

namespace App\Repository;

use App\Entity\VdViajesEstadosPanico;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class VdViajesEstadosPanicoRepository  extends EntityRepository
{
    public function save(VdViajesEstadosPanico $entity) {
        $em = $this->getEntityManager();
        try {
            $em->getConnection()->beginTransaction();
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

    public function getPanicStateByTravel($travelId) {
        try {
            $qb = $this->createQueryBuilder('ps')
                ->innerJoin("ps.viaje","v")
                ->innerJoin("ps.estadoPanico","s")
                ->orderBy("ps.id", "DESC");

            $qb->where($qb->expr()->eq('v.id', $travelId ));
            return $qb->select('ps,s')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }
}
