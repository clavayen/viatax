<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/10/18
 * Time: 17:45
 */

namespace App\Repository;

use App\Entity\VdUsuariosProtectores;
use App\Exception\ProtectorPendingAcceptanceException;
use App\Exception\ProtectorWasAddedException;
use App\Utils\Codes;
use App\Utils\Constants;
use Doctrine\ORM\EntityRepository;
use App\Pagination\ListadoPaginar;
use \Exception;


class UserProtectorRepository extends EntityRepository{

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder("up")
            ->innerJoin("up.usuario","u")
            ->innerJoin("up.protector",'p')
            ->where("up.estado = :pEstado")
            ->setParameter("pEstado",Constants::PROTECTOR_ADDED);


        if (!empty($userId)) {
            $qb->andWhere($qb->expr()->eq('u.id', $userId ));
        }
        if (!empty($protectorId)) {
            $qb->andWhere($qb->expr()->eq('p.id', $protectorId ));
        }
        if (!empty($relation)) {
            $qb->andWhere($qb->expr()->like('up.relacion', "'".$relation."'" ));
        }
        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id','relacion')) ? $sortField : 'id';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(up)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('up,p')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function searchRequest($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder("up")
            ->innerJoin("up.usuario","u")
            ->innerJoin("up.protector",'p')
            ->where("up.estado = :pEstado")
            ->setParameter("pEstado",Constants::PROTECTOR_PENDING);

        if (!empty($userId)) {
            $qb->andWhere($qb->expr()->eq('u.id', $userId ));
        }
        if (!empty($protectorId)) {
            $qb->andWhere($qb->expr()->eq('p.id', $protectorId ));
        }
        if (!empty($relation)) {
            $qb->andWhere($qb->expr()->like('up.relacion', "'".$relation."'" ));
        }
        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id','relacion')) ? $sortField : 'id';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(up)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('up,u')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function searchMyProtected($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder("up")
            ->innerJoin("up.usuario","u")
            ->innerJoin("up.protector",'p')
            ->where("up.estado = :pEstado")
            ->setParameter("pEstado",Constants::PROTECTOR_ADDED);

        if (!empty($userId)) {
            $qb->andWhere($qb->expr()->eq('u.id', $userId ));
        }
        if (!empty($protectorId)) {
            $qb->andWhere($qb->expr()->eq('p.id', $protectorId ));
        }
        if (!empty($relation)) {
            $qb->andWhere($qb->expr()->like('up.relacion', "'".$relation."'" ));
        }
        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id','relacion')) ? $sortField : 'id';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(up)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('up,u')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function get($id) {
        try {
            $qb = $this->createQueryBuilder('u');
            $qb->where($qb->expr()->eq('u.id', $id ));
            $result = $qb->select('u')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            return $result;
        } catch (Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }

    public function save(VdUsuariosProtectores $entity) {
        $em = $this->getEntityManager();
        try {

            $em->getConnection()->beginTransaction();

            $this->validateProtectorNotAdded($entity);

            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        } catch (ProtectorPendingAcceptanceException $e) {
            $em->getConnection()->rollback();
            throw new \Exception("La solicitud ya fue enviado al usuario y se encuentra en espera de aceptacion", Codes::ALREADY_INFORMATED);
        } catch (ProtectorWasAddedException $e) {
            $em->getConnection()->rollback();
            throw new \Exception("El usuario ya se encuentra entre sus protectores. Elija otro por favor..", Codes::ALREADY_INFORMATED);
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        return null;
    }

    private function validateProtectorNotAdded(VdUsuariosProtectores $entity){
        $query = $this->createQueryBuilder('s')
            ->innerJoin("s.usuario","u")
            ->innerJoin("s.protector","p")
            ->where('u.id = :pUser')
            ->setParameter('pUser', $entity->getUsuario()->getId())
            ->andWhere('p.id = :pProtector')
            ->setParameter('pProtector', $entity->getProtector()->getId())
            ->getQuery();

        $protector = $query->getOneOrNullResult();

        if(is_null($protector)){
            return;
        }else{
            $state = $protector->getEstado();
        }

        switch ($state) {
            case Constants::PROTECTOR_PENDING:
                    throw new ProtectorPendingAcceptanceException();
                break;
            case Constants::PROTECTOR_ADDED:
                    throw new ProtectorWasAddedException();
                break;
        }
    }

    public function confirmRequest(VdUsuariosProtectores $entity){
        $em = $this->getEntityManager();
        try {
            $em->getConnection()->beginTransaction();
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        return $entity;
    }

    public function rejectRequest(VdUsuariosProtectores $entity){
        $em = $this->getEntityManager();
        try {
            $em->remove($entity);
            $em->flush();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

}