<?php

namespace App\Repository;

use App\Entity\VdEstadosPanico;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VdEstadosPanico|null find($id, $lockMode = null, $lockVersion = null)
 * @method VdEstadosPanico|null findOneBy(array $criteria, array $orderBy = null)
 * @method VdEstadosPanico[]    findAll()
 * @method VdEstadosPanico[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VdEstadosPanicosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VdEstadosPanico::class);
    }

    // /**
    //  * @return VdEstadosPanico[] Returns an array of VdEstadosPanico objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VdEstadosPanico
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
