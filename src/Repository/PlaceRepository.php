<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/10/18
 * Time: 19:03
 */

namespace App\Repository;

use App\Entity\VdLugaresFrecuentes;
use App\Exception\NameDuplicatedException;
use App\Utils\Codes;
use App\Utils\Constants;
use Doctrine\ORM\EntityRepository;
use App\Pagination\ListadoPaginar;
use Doctrine\ORM\NoResultException;

class PlaceRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder("f")
            ->innerJoin("f.usuario","u");

        if (!empty($userId)) {
            $qb->andWhere($qb->expr()->eq('u.id', $userId ));
        }
        if (!empty($description)) {
            $qb->andWhere($qb->expr()->like('f.descripcion', "'".$description."'" ));
        }
        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id','descripcion')) ? $sortField : 'id';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(f)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('f')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function get($id) {
        try {
            $qb = $this->createQueryBuilder('u');
            $qb->where($qb->expr()->eq('u.id', $id ));
            $result = $qb->select('u')->getQuery()->getOneOrNullResult();
            return $result;
        } catch (Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }

    public function save(VdLugaresFrecuentes $entity) {
        $em = $this->getEntityManager();
        try {

            $em->getConnection()->beginTransaction();

            if($entity->getId() === null){
                $codeAction = Constants::FORMAT_REGISTER;
            }else{
                $codeAction = Constants::FORMAT_EDIT;
            }

            $this->validateUniqueName($entity,$codeAction);

            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        } catch (NameDuplicatedException $e) {
            $em->getConnection()->rollback();
            throw new \Exception("El nombre ingresado ya existe.", Codes::CONFLICT);
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        return null;
    }

    public function delete(VdLugaresFrecuentes $entity){
        $em = $this->getEntityManager();
        try {
            $em->remove($entity);
            $em->flush();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
    }

    private function validateUniqueName(VdLugaresFrecuentes $entity , $codeAction){
        $query = $this->createQueryBuilder('s')
            ->innerJoin("s.usuario","u")
            ->where('s.descripcion = :pDescription')
            ->setParameter('pDescription', $entity->getDescripcion())
            ->andWhere('u.id = :pUser')
            ->setParameter('pUser', $entity->getUsuario()->getId())
            ->getQuery();
        try {
            $duplicated = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        }catch (NoResultException  $e){
            $duplicated = null;
        }

        switch ($codeAction) {
            case Constants::FORMAT_REGISTER:
                if($duplicated)
                    throw new NameDuplicatedException();
                break;
            case Constants::FORMAT_EDIT:
                if($duplicated){
                    if($entity->getId() != $duplicated['id']  )
                        throw new NameDuplicatedException();
                }
                break;
        }
    }

    public function getAllByUser($id) {
        try {
            $qb = $this->createQueryBuilder('p')
                ->innerJoin("p.usuario","u");
            $qb->where($qb->expr()->eq('u.id', $id ));
            $result = $qb->select('p')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            return $result;
        } catch (Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }

}