<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 14/01/19
 * Time: 20:41
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class UserTaxiRepository extends EntityRepository {

    public function getByTaxi($id) {
        try {
            $qb = $this->createQueryBuilder('ut')
                ->innerJoin("ut.taxi","t")
                ->innerJoin("ut.usuario","u");

            $qb->where($qb->expr()->eq('t.id', $id ));
            $result = $qb->select('ut')->getQuery()->getOneOrNullResult();
            return $result;
        } catch (\Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }

}