<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use App\Entity\VdUsuarios;
use App\Exception\EmailDuplicatedException;
use App\Exception\NameDuplicatedException;
use App\Utils\Constants;
use Doctrine\ORM\EntityRepository;
use App\Pagination\ListadoPaginar;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use App\Utils\Codes;

/**
 * Description of UsuariosRepository
 *
 * @author coco
 */
class UserRepository extends EntityRepository{

    public function login($params) {
        extract($params);
        return $this->findOneBy(array("usuario" => $username, "password" => $password));
    }
    
    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        extract($searchParam);
        
        $qb = $this->createQueryBuilder('u');

        if (!empty($id)) {
            $qb->andWhere($qb->expr()->eq('u.id', $id ));
        }
        if (!empty($usuario)) {
            $qb->andWhere($qb->expr()->like('u.usuario', "'".$usuario."'" ));
        }
        if (!empty($nombre)) {
            $qb->andWhere($qb->expr()->like('u.nombre', "'".$nombre."'" ));
        }
        if (!empty($apellido)) {
            $qb->andWhere($qb->expr()->like('u.apellido', "'".$apellido."'" ));
        }
        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id','nombre','apellido')) ? $sortField : 'apellido';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $dql = $qb->getDQL();
        $query = $qb->getQuery();
        $sql = $query->getSQL();

        $numElementos = $qb->select('COUNT(u)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('u')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }
    
    public function get($id) {
        try {
            $qb = $this->createQueryBuilder('u');
            $qb->where($qb->expr()->eq('u.id', $id ));
            $result = $qb->select('u')->getQuery()->getOneOrNullResult();
            return $result;
        } catch (Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        
    }

    public function searchDynamic($param) {
        $qb = $this->createQueryBuilder('u')
                    ->select("u.id, u.nombre, u.apellido, u.usuario, u.email, u.direccion, u.telefono, u.token");

        if (!empty($param)) {
            $qb->where($qb->expr()->orX(
                $qb->expr()->like('u.nombre', "'%".$param."%'" ),
                $qb->expr()->like('u.apellido', "'%".$param."%'" ),
                $qb->expr()->like('u.usuario', "'%".$param."%'" )
            ));
        }
        $qb->orderBy('u.apellido', "DESC");
        $results = $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $results;
    }

    public function save(VdUsuarios $entity) {
        $em = $this->getEntityManager();
        try {

            $em->getConnection()->beginTransaction();

            if($entity->getId() === null){
                $codeAction = Constants::FORMAT_REGISTER;
            }else{
                $codeAction = Constants::FORMAT_EDIT;
            }

            $this->validateUniqueUsername($entity,$codeAction);
            $this->validateUniqueEmail($entity,$codeAction);

            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
//
        } catch (NameDuplicatedException $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Ya existe un usuario con ese nombre. Por favor ingrese otro.",Codes::CONFLICT);
        } catch (EmailDuplicatedException $e) {
            $em->getConnection()->rollback();
            throw new \Exception("El email ingresado ya se encuentra registrado. Por favor ingrese otro.",Codes::CONFLICT);
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        return null;
    }

    private function validateUniqueUsername(VdUsuarios $entity , $codeAction){
        $query = $this->createQueryBuilder('s')
            ->where('s.usuario = :pUsername')
            ->setParameter('pUsername', $entity->getUsuario())
            ->getQuery();
        try {
            $duplicated = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        }catch (NoResultException  $e){
            $duplicated = null;
        }

        switch ($codeAction) {
            case Constants::FORMAT_REGISTER:
                if($duplicated)
                    throw new NameDuplicatedException();
                break;
            case Constants::FORMAT_EDIT:
                if($duplicated){
                    if($entity->getId() != $duplicated['id']  )
                        throw new NameDuplicatedException();
                }
                break;
        }
    }

    private function validateUniqueEmail(VdUsuarios $entity , $codeAction){
        $query = $this->createQueryBuilder('s')
            ->where('s.email = :pEmail')
            ->setParameter('pEmail', $entity->getEmail())
            ->getQuery();
        try {
            $duplicated = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        }catch (NoResultException  $e){
            $duplicated = null;
        }

        switch ($codeAction) {
            case Constants::FORMAT_REGISTER:
                if($duplicated)
                    throw new EmailDuplicatedException();
                break;
            case Constants::FORMAT_EDIT:
                if($duplicated){
                    if($entity->getId() != $duplicated['id']  )
                        throw new EmailDuplicatedException();
                }
                break;
        }
    }
}
