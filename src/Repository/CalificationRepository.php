<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 14/01/19
 * Time: 19:01
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use App\Utils\Codes;
use App\Utils\Constants;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;


class CalificationRepository extends EntityRepository {

    public function getQualificationByTaxi($taxiId) {
        $qb = $this->createQueryBuilder('u')
            ->select('avg(u.cortesia) as cortesy',
                'avg(u.conduccion) as driving',
                'avg(u.estadoVehiculo) as carState',
                'avg(u.rutaSeleccionada) as routeSelected',
                'avg(u.total) as total')
            ->innerJoin("u.viaje","v")
            ->innerJoin("v.taxi","t")
            ->where("t.id = :pTaxiId")
            ->setParameter("pTaxiId",$taxiId);
        $result = $qb->getQuery()->getResult();
        return (count($result) > 0)?$result[0]:null;
    }

}