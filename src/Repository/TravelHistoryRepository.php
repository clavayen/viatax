<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 18/03/19
 * Time: 03:18
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class TravelHistoryRepository extends EntityRepository {

    public function getByTravelId($travelId) {
        $qb = $this->createQueryBuilder('u')
            ->select('u.id,u.latitud,u.longitud,u.fecha')
            ->innerJoin("u.viaje","v")
            ->orderBy("u.id");
        $qb->where($qb->expr()->eq('v.id',$travelId ));
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

}