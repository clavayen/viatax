<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use App\Entity\VdTaxis;
use App\Exception\PatentDuplicatedException;
use App\Exception\TagDuplicatedException;
use App\Pagination\ListadoPaginar;
use App\Utils\Codes;
use App\Utils\Constants;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use \Exception;

/**
 * Description of UsuariosRepository
 *
 * @author coco
 */
class TaxiRepository extends EntityRepository{

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        extract($searchParam);

        $qb = $this->createQueryBuilder('u');

        if (!empty($id)) {
            $qb->andWhere($qb->expr()->eq('u.id', $id ));
        }
        if (!empty($usuario)) {
            $qb->andWhere($qb->expr()->like('u.marca', "'".$usuario."'" ));
        }
        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id','nombre','apellido')) ? $sortField : 'apellido';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(u)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('u')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }
    
    public function get($id) {
        try {
            $qb = $this->createQueryBuilder('u');
            $qb->where($qb->expr()->eq('u.id', $id ));
            $result = $qb->select('u')->getQuery()->getOneOrNullResult();
            return $result;
        } catch (\Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        
    }

    public function save(VdTaxis $entity) {
        $em = $this->getEntityManager();
        try {

            $em->getConnection()->beginTransaction();

            if($entity->getId() === null){
                $codeAction = Constants::FORMAT_REGISTER;
            }else{
                $codeAction = Constants::FORMAT_EDIT;
            }

            $this->validateUniquePatent($entity,$codeAction);
            $this->validateUniqueTag($entity,$codeAction);

            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        } catch (PatentDuplicatedException $e) {
            $em->getConnection()->rollback();
            throw new \Exception("El numero de patente ya se encuentra registrada. Por favor ingrese otro.", Codes::CONFLICT);
        }catch (TagDuplicatedException $e) {
                $em->getConnection()->rollback();
                throw new \Exception("El Tag NFC ya se encuentra registrado. Por favor ingrese otro.",Codes::CONFLICT);
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        return null;
    }

    private function validateUniquePatent(VdTaxis $entity , $codeAction){
        $query = $this->createQueryBuilder('s')
            ->where('s.patente = :pPatent')
            ->setParameter('pPatent', $entity->getPatente())
            ->getQuery();
        try {
            $duplicated = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        }catch (NoResultException  $e){
            $duplicated = null;
        }

        switch ($codeAction) {
            case Constants::FORMAT_REGISTER:
                if($duplicated)
                    throw new PatentDuplicatedException();
                break;
            case Constants::FORMAT_EDIT:
                if($duplicated){
                    if($entity->getId() != $duplicated['id']  )
                        throw new PatentDuplicatedException();
                }
                break;
        }
    }

    private function validateUniqueTag(VdTaxis $entity , $codeAction){
        $query = $this->createQueryBuilder('s')
            ->where('s.tag = :pTag')
            ->setParameter('pTag', $entity->getTag())
            ->getQuery();
        try {
            $duplicated = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        }catch (NoResultException  $e){
            $duplicated = null;
        }

        switch ($codeAction) {
            case Constants::FORMAT_REGISTER:
                if($duplicated)
                    throw new TagDuplicatedException();
                break;
            case Constants::FORMAT_EDIT:
                if($duplicated){
                    if($entity->getId() != $duplicated['id']  )
                        throw new TagDuplicatedException();
                }
                break;
        }
    }

    public function getFull($id) {
        try {
            $qb = $this->createQueryBuilder('ut')
                ->from('App\Entity\VdUsuariosTaxis','ut')
                ->innerJoin("ut.taxi","t")
                ->innerJoin("ut.usuario","u");

            $qb->where($qb->expr()->eq('t.id', $id ));
            $result = $qb->select('ut')->getQuery()->getOneOrNullResult();
            return $result;
        } catch (\Exception $e) {
            throw new Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }

    }
}
