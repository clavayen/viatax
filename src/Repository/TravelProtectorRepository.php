<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 06/05/19
 * Time: 20:33
 */

namespace App\Repository;

use App\Entity\VdViajesProtectores;
use Doctrine\ORM\EntityRepository;
use App\Pagination\ListadoPaginar;
use Doctrine\ORM\Query;


class TravelProtectorRepository extends EntityRepository {

    public function searchByProtector($first, $max, $sortField, $sortDirection, $searchParam, $accepted, $state) {
        extract($searchParam);

        $qb = $this->createQueryBuilder('tp')
            ->innerJoin("tp.protector","p")
            ->innerJoin("tp.viaje","t")
            ->innerJoin("t.estadoViaje","e")
            ->innerJoin("t.usuario","u")
            ->innerJoin("t.taxi", "a")
            ->where("tp.aceptado = :pAccepted")
            ->setParameter("pAccepted", $accepted);

        if (!empty($protectorId)) {
            $qb->andWhere($qb->expr()->eq('p.id', $protectorId ));
        }

        if (!is_null($state)) {
            $qb->andWhere($qb->expr()->eq('e.nombre', "'".$state."'" ));
        }

        if (!empty($userId)) {
            $qb->andWhere($qb->expr()->eq('u.id', $userId ));
        }

        if (!empty($travelId)) {
            $qb->andWhere($qb->expr()->eq('t.id', $travelId ));
        }

        if(!empty($sortField)){
            $sortField = in_array($sortField, array('id')) ? $sortField : 'id';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('tp.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(tp)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('tp,p,t,u,e,a')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function save(VdViajesProtectores $entity) {
        $em = $this->getEntityManager();
        try {
            $em->getConnection()->beginTransaction();
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();
        }catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Exception("Hubo un error:" . $e->getMessage(), $e->getCode());
        }
        return null;
    }

}