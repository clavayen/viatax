<?php
namespace App\Dto;

class ResponseDto {
	private $data;
        private $code;
        private $status;
        private $message;
	
	function __construct(){
	}
	
        function getData() {
            return $this->data;
        }

        function getStatus() {
            return $this->status;
        }

        function getMessage() {
            return $this->message;
        }
        function getCode() {
            return $this->code;
        }

        function setData($data) {
            $this->data = $data;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setMessage($message) {
            $this->message = $message;
        }

        function setCode($code) {
            $this->code = $code;
        }
	
}