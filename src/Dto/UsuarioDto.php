<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 20/07/18
 * Time: 00:51
 */

namespace App\Dto;

/**
 * UsuarioDto
 */

class UsuarioDto
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $usuario;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $apellido;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsuario(): string
    {
        return $this->usuario;
    }

    /**
     * @param string $usuario
     */
    public function setUsuario(string $usuario): void
    {
        $this->usuario = $usuario;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApellido(): string
    {
        return $this->apellido;
    }

    /**
     * @param string $apellido
     */
    public function setApellido(string $apellido): void
    {
        $this->apellido = $apellido;
    }



}