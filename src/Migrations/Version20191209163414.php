<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191209163414 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vd_estados_panico (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(50) NOT NULL, descripcion VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_viajes_estados_panico (id INT AUTO_INCREMENT NOT NULL, viaje_id INT NOT NULL, estado_panico_id INT NOT NULL, latitud VARCHAR(255) NOT NULL, longitud VARCHAR(255) NOT NULL, fecha_creacion DATETIME NOT NULL, INDEX IDX_52C148C994E1E648 (viaje_id), INDEX IDX_52C148C940480E8 (estado_panico_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vd_viajes_estados_panico ADD CONSTRAINT FK_52C148C994E1E648 FOREIGN KEY (viaje_id) REFERENCES vd_viajes (id)');
        $this->addSql('ALTER TABLE vd_viajes_estados_panico ADD CONSTRAINT FK_52C148C940480E8 FOREIGN KEY (estado_panico_id) REFERENCES vd_estados_panico (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vd_viajes_estados_panico DROP FOREIGN KEY FK_52C148C940480E8');
        $this->addSql('DROP TABLE vd_estados_panico');
        $this->addSql('DROP TABLE vd_viajes_estados_panico');
    }
}
