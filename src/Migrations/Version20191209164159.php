<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191209164159 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `vd_roles` VALUES (1,\'USUARIO\',\'ROLE_USUARIO\'),(2,\'TAXISTA\',\'ROLE_TAXISTA\');');
        $this->addSql('INSERT INTO `vd_usuarios` (`id`, `usuario`, `password`, `nombre`, `apellido`, `email`, `fecha_creacion`, `direccion`, `telefono`, `foto`, `token`) VALUES
                            (1, \'jdocile\', \'1234\', \'Juan Manuel\', \'Docile\', \'jdocile@gmail.com\', \'2019-02-01 04:44:35\', \'Cabo Primero\', \'3885130252\', NULL, \'\'),
                            (2, \'halvarado\', \'1234\', \'Horacio Humberto\', \'Alvarado\', \'halvarado@gmail.com\', \'2019-02-07 13:43:08\', \'Italo Palanca\', \'424242424\', NULL, \'\');');
        $this->addSql('INSERT INTO `vd_choferes` (`id`, `nombre`, `apellido`, `domicilio`, `edad`, `fecha_creacion`) VALUES
                            (1, \'Hector\', \'Bitancur\', \'Juana Manuela Gorriti N° 247\', 25, \'2018-10-10 00:00:00\');');
        $this->addSql('INSERT INTO `vd_estados_viajes` (`id`, `nombre`, `descripcion`) VALUES
                            (1, \'INICIADO\', \'Viaje Inciado\'),
                            (2, \'FINALIZADO\', \'Viaje Finalizado\'),
                            (3, \'INCONCLUSO\', \'Viaje No Finalizado\');');
        $this->addSql('INSERT INTO `vd_estados_panico` (`id`, `nombre`, `descripcion`) VALUES
                            (1, \'STATE_HAPPY\', \'Mi viaje es agradable.\'),
                            (2, \'STATE_WARNING\', \'Por favor no te descuides de mi viaje, sigue mi trayecto.\'),
                            (3, \'STATE_DANGER\', \'Estoy teniendo un problema con mi viaje. Necesito ayuda.\');');
        $this->addSql('INSERT INTO `vd_taxis` (`id`, `patente`, `modelo`, `marca`, `anio`, `descripcion`, `empresa`, `fecha_creacion`, `tag`) VALUES
                            (1, \'FSG-008\', \'Gol Power 1.6\', \'VOLSKWAGEN\', \'2006\', \'Polarizado, en condiciones Correctas\', \'RIO BALNCO\', \'2018-05-01 00:00:00\', \'12345\'),
                            (2, \'LNK-997\', \'207 Allure\', \'Peugeot\', \'2012\', \'Descripcion\', \'Remises Centro\', \'2018-11-09 14:53:43\', \'123456\');');
        $this->addSql('INSERT INTO `vd_taxis_choferes` VALUES (1,1,1);');
        $this->addSql('INSERT INTO `vd_usuarios_taxis` (`id`, `usuario_id`, `taxi_id`) VALUES
                            (1, 1, 1),(2, 2, 2);');
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
