<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191205000017 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vd_usuarios_protectores (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, protector_id INT DEFAULT NULL, estado TINYINT(1) DEFAULT NULL, fecha_creacion DATETIME NOT NULL, fecha_aceptacion DATETIME DEFAULT NULL, relacion VARCHAR(100) NOT NULL, INDEX IDX_7410B256DB38439E (usuario_id), INDEX IDX_7410B25685C04254 (protector_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_calificaciones (id INT AUTO_INCREMENT NOT NULL, viaje_id INT DEFAULT NULL, cortesia NUMERIC(10, 2) DEFAULT NULL, estado_vehiculo NUMERIC(10, 2) DEFAULT NULL, ruta_seleccionada NUMERIC(10, 2) DEFAULT NULL, conduccion NUMERIC(10, 2) DEFAULT NULL, total NUMERIC(10, 2) DEFAULT NULL, fecha DATETIME DEFAULT CURRENT_TIMESTAMP, INDEX IDX_AD423B6694E1E648 (viaje_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_viajes_historial (id INT AUTO_INCREMENT NOT NULL, viaje_id INT DEFAULT NULL, latitud VARCHAR(45) NOT NULL, longitud VARCHAR(45) NOT NULL, canal_socket VARCHAR(45) NOT NULL, fecha DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_9D9B09C594E1E648 (viaje_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_choferes (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) NOT NULL, apellido VARCHAR(100) NOT NULL, domicilio VARCHAR(100) NOT NULL, edad INT DEFAULT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_usuarios (id INT AUTO_INCREMENT NOT NULL, usuario VARCHAR(45) NOT NULL, password VARCHAR(45) NOT NULL, nombre VARCHAR(100) NOT NULL, apellido VARCHAR(100) NOT NULL, email VARCHAR(45) NOT NULL COMMENT \'EL CORREO ES UNICO\', fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP, direccion VARCHAR(255) NOT NULL, telefono VARCHAR(100) NOT NULL, token VARCHAR(255) DEFAULT NULL, foto VARCHAR(100) NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_viajes_protectores (id INT AUTO_INCREMENT NOT NULL, viaje_id INT DEFAULT NULL, protector_id INT DEFAULT NULL, aceptado TINYINT(1) DEFAULT NULL, fecha_aceptacion DATETIME DEFAULT NULL, INDEX IDX_DC6C615194E1E648 (viaje_id), INDEX IDX_DC6C615185C04254 (protector_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_notificaciones (id INT AUTO_INCREMENT NOT NULL, viaje_id INT DEFAULT NULL, protector_id INT DEFAULT NULL, notificacion_recibida TINYINT(1) DEFAULT \'1\' COMMENT \'1: NO RECIBIDA, 2: RECIBIDA\', eliminado TINYINT(1) DEFAULT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP, INDEX IDX_EA4ADC8F94E1E648 (viaje_id), INDEX IDX_EA4ADC8F85C04254 (protector_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_roles (id INT AUTO_INCREMENT NOT NULL, descripcion VARCHAR(45) NOT NULL, nombre_alternativo VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_viajes (id INT AUTO_INCREMENT NOT NULL, estado_viaje_id INT DEFAULT NULL, usuario_id INT DEFAULT NULL, taxi_id INT DEFAULT NULL, lugar_frecuente_id INT DEFAULT NULL, nro_viaje VARCHAR(45) NOT NULL, latitud_inicio VARCHAR(45) NOT NULL, longitud_inicio VARCHAR(45) NOT NULL, latitud_fin VARCHAR(45) DEFAULT NULL, longitud_fin VARCHAR(45) DEFAULT NULL, canal_socket VARCHAR(45) NOT NULL, calificacion VARCHAR(45) DEFAULT NULL, cantidad_avisos INT DEFAULT NULL COMMENT \'es para saber la cantidad de avisos que emitio la aplicacion para saber si termino el viaje\', fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, descripcion VARCHAR(150) DEFAULT NULL, fecha_finalizacion DATETIME DEFAULT NULL, precio VARCHAR(45) NULL, tiempo_estimado VARCHAR(45) NULL, distancia_estimada VARCHAR(45) NULL, INDEX IDX_BA8903FA651755A5 (estado_viaje_id), INDEX IDX_BA8903FADB38439E (usuario_id), INDEX IDX_BA8903FA506FF81C (taxi_id), INDEX IDX_BA8903FAB3DEBDB2 (lugar_frecuente_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_usuarios_taxis (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, taxi_id INT DEFAULT NULL, INDEX IDX_FAB9A327DB38439E (usuario_id), INDEX IDX_FAB9A327506FF81C (taxi_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_lugares_frecuentes (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, latitud VARCHAR(45) NOT NULL, longitud VARCHAR(100) NOT NULL, descripcion VARCHAR(100) NOT NULL, estado TINYINT(1) DEFAULT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP, direccion VARCHAR(255) NOT NULL, numero VARCHAR(45) NOT NULL, localidad VARCHAR(150) NOT NULL, INDEX IDX_6491006BDB38439E (usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_taxis_choferes (id INT AUTO_INCREMENT NOT NULL, taxi_id INT DEFAULT NULL, chofer_id INT DEFAULT NULL, INDEX IDX_270420B4506FF81C (taxi_id), INDEX IDX_270420B42A5E4E82 (chofer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_usuarios_roles (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, rol_id INT DEFAULT NULL, INDEX IDX_22BC6514DB38439E (usuario_id), INDEX IDX_22BC65144BAB96C (rol_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_estados_viajes (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(45) NOT NULL, descripcion VARCHAR(150) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vd_taxis (id INT AUTO_INCREMENT NOT NULL, patente VARCHAR(45) NOT NULL, modelo VARCHAR(100) NOT NULL, marca VARCHAR(100) NOT NULL, descripcion VARCHAR(100) DEFAULT NULL, empresa VARCHAR(100) DEFAULT NULL, fecha_creacion DATETIME NOT NULL, tag VARCHAR(100) DEFAULT NULL, anio VARCHAR(8) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vd_usuarios_protectores ADD CONSTRAINT FK_7410B256DB38439E FOREIGN KEY (usuario_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_usuarios_protectores ADD CONSTRAINT FK_7410B25685C04254 FOREIGN KEY (protector_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_calificaciones ADD CONSTRAINT FK_AD423B6694E1E648 FOREIGN KEY (viaje_id) REFERENCES vd_viajes (id)');
        $this->addSql('ALTER TABLE vd_viajes_historial ADD CONSTRAINT FK_9D9B09C594E1E648 FOREIGN KEY (viaje_id) REFERENCES vd_viajes (id)');
        $this->addSql('ALTER TABLE vd_viajes_protectores ADD CONSTRAINT FK_DC6C615194E1E648 FOREIGN KEY (viaje_id) REFERENCES vd_viajes (id)');
        $this->addSql('ALTER TABLE vd_viajes_protectores ADD CONSTRAINT FK_DC6C615185C04254 FOREIGN KEY (protector_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_notificaciones ADD CONSTRAINT FK_EA4ADC8F94E1E648 FOREIGN KEY (viaje_id) REFERENCES vd_viajes (id)');
        $this->addSql('ALTER TABLE vd_notificaciones ADD CONSTRAINT FK_EA4ADC8F85C04254 FOREIGN KEY (protector_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_viajes ADD CONSTRAINT FK_BA8903FA651755A5 FOREIGN KEY (estado_viaje_id) REFERENCES vd_estados_viajes (id)');
        $this->addSql('ALTER TABLE vd_viajes ADD CONSTRAINT FK_BA8903FADB38439E FOREIGN KEY (usuario_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_viajes ADD CONSTRAINT FK_BA8903FA506FF81C FOREIGN KEY (taxi_id) REFERENCES vd_taxis (id)');
        $this->addSql('ALTER TABLE vd_viajes ADD CONSTRAINT FK_BA8903FAB3DEBDB2 FOREIGN KEY (lugar_frecuente_id) REFERENCES vd_lugares_frecuentes (id)');
        $this->addSql('ALTER TABLE vd_usuarios_taxis ADD CONSTRAINT FK_FAB9A327DB38439E FOREIGN KEY (usuario_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_usuarios_taxis ADD CONSTRAINT FK_FAB9A327506FF81C FOREIGN KEY (taxi_id) REFERENCES vd_taxis (id)');
        $this->addSql('ALTER TABLE vd_lugares_frecuentes ADD CONSTRAINT FK_6491006BDB38439E FOREIGN KEY (usuario_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_taxis_choferes ADD CONSTRAINT FK_270420B4506FF81C FOREIGN KEY (taxi_id) REFERENCES vd_taxis (id)');
        $this->addSql('ALTER TABLE vd_taxis_choferes ADD CONSTRAINT FK_270420B42A5E4E82 FOREIGN KEY (chofer_id) REFERENCES vd_choferes (id)');
        $this->addSql('ALTER TABLE vd_usuarios_roles ADD CONSTRAINT FK_22BC6514DB38439E FOREIGN KEY (usuario_id) REFERENCES vd_usuarios (id)');
        $this->addSql('ALTER TABLE vd_usuarios_roles ADD CONSTRAINT FK_22BC65144BAB96C FOREIGN KEY (rol_id) REFERENCES vd_roles (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vd_taxis_choferes DROP FOREIGN KEY FK_270420B42A5E4E82');
        $this->addSql('ALTER TABLE vd_usuarios_protectores DROP FOREIGN KEY FK_7410B256DB38439E');
        $this->addSql('ALTER TABLE vd_usuarios_protectores DROP FOREIGN KEY FK_7410B25685C04254');
        $this->addSql('ALTER TABLE vd_viajes_protectores DROP FOREIGN KEY FK_DC6C615185C04254');
        $this->addSql('ALTER TABLE vd_notificaciones DROP FOREIGN KEY FK_EA4ADC8F85C04254');
        $this->addSql('ALTER TABLE vd_viajes DROP FOREIGN KEY FK_BA8903FADB38439E');
        $this->addSql('ALTER TABLE vd_usuarios_taxis DROP FOREIGN KEY FK_FAB9A327DB38439E');
        $this->addSql('ALTER TABLE vd_lugares_frecuentes DROP FOREIGN KEY FK_6491006BDB38439E');
        $this->addSql('ALTER TABLE vd_usuarios_roles DROP FOREIGN KEY FK_22BC6514DB38439E');
        $this->addSql('ALTER TABLE vd_usuarios_roles DROP FOREIGN KEY FK_22BC65144BAB96C');
        $this->addSql('ALTER TABLE vd_calificaciones DROP FOREIGN KEY FK_AD423B6694E1E648');
        $this->addSql('ALTER TABLE vd_viajes_historial DROP FOREIGN KEY FK_9D9B09C594E1E648');
        $this->addSql('ALTER TABLE vd_viajes_protectores DROP FOREIGN KEY FK_DC6C615194E1E648');
        $this->addSql('ALTER TABLE vd_notificaciones DROP FOREIGN KEY FK_EA4ADC8F94E1E648');
        $this->addSql('ALTER TABLE vd_viajes DROP FOREIGN KEY FK_BA8903FAB3DEBDB2');
        $this->addSql('ALTER TABLE vd_viajes DROP FOREIGN KEY FK_BA8903FA651755A5');
        $this->addSql('ALTER TABLE vd_viajes DROP FOREIGN KEY FK_BA8903FA506FF81C');
        $this->addSql('ALTER TABLE vd_usuarios_taxis DROP FOREIGN KEY FK_FAB9A327506FF81C');
        $this->addSql('ALTER TABLE vd_taxis_choferes DROP FOREIGN KEY FK_270420B4506FF81C');
        $this->addSql('DROP TABLE vd_usuarios_protectores');
        $this->addSql('DROP TABLE vd_calificaciones');
        $this->addSql('DROP TABLE vd_viajes_historial');
        $this->addSql('DROP TABLE vd_choferes');
        $this->addSql('DROP TABLE vd_usuarios');
        $this->addSql('DROP TABLE vd_viajes_protectores');
        $this->addSql('DROP TABLE vd_notificaciones');
        $this->addSql('DROP TABLE vd_roles');
        $this->addSql('DROP TABLE vd_viajes');
        $this->addSql('DROP TABLE vd_usuarios_taxis');
        $this->addSql('DROP TABLE vd_lugares_frecuentes');
        $this->addSql('DROP TABLE vd_taxis_choferes');
        $this->addSql('DROP TABLE vd_usuarios_roles');
        $this->addSql('DROP TABLE vd_estados_viajes');
        $this->addSql('DROP TABLE vd_taxis');
    }
}
