-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: viatax
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vd_calificaciones`
--

DROP TABLE IF EXISTS `vd_calificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_calificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxi_id` int(11) NOT NULL,
  `indicativo_id` int(11) NOT NULL,
  `calificacion` decimal(10,2) NOT NULL,
  `viaje_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `fecha_calificacion` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_calificaciones_1_idx` (`taxi_id`),
  KEY `fk_calificaciones_2_idx` (`indicativo_id`),
  KEY `fk_calificaciones_3_idx` (`viaje_id`),
  KEY `fk_calificaciones_4_idx` (`usuario_id`),
  CONSTRAINT `fk_calificaciones_1` FOREIGN KEY (`taxi_id`) REFERENCES `vd_taxis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calificaciones_2` FOREIGN KEY (`indicativo_id`) REFERENCES `vd_indicativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calificaciones_3` FOREIGN KEY (`viaje_id`) REFERENCES `vd_viajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calificaciones_4` FOREIGN KEY (`usuario_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_calificaciones`
--

LOCK TABLES `vd_calificaciones` WRITE;
/*!40000 ALTER TABLE `vd_calificaciones` DISABLE KEYS */;
INSERT INTO `vd_calificaciones` VALUES (1,2,1,3.50,6,1,0,'2018-12-12 00:00:00'),(2,2,2,4.10,6,1,0,'2018-12-12 00:00:00'),(3,2,3,2.70,6,1,0,'2018-12-12 00:00:00'),(4,2,4,3.90,6,1,0,'2018-12-12 00:00:00'),(5,2,1,5.00,6,1,0,'2018-12-12 00:00:00');
/*!40000 ALTER TABLE `vd_calificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_choferes`
--

DROP TABLE IF EXISTS `vd_choferes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_choferes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `domicilio` varchar(100) NOT NULL,
  `edad` int(4) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_choferes`
--

LOCK TABLES `vd_choferes` WRITE;
/*!40000 ALTER TABLE `vd_choferes` DISABLE KEYS */;
INSERT INTO `vd_choferes` VALUES (1,'Hector','Bitancur','Juana Manuela Gorriti N° 247',25,'2018-10-10 00:00:00');
/*!40000 ALTER TABLE `vd_choferes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_estados_viajes`
--

DROP TABLE IF EXISTS `vd_estados_viajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_estados_viajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_estados_viajes`
--

LOCK TABLES `vd_estados_viajes` WRITE;
/*!40000 ALTER TABLE `vd_estados_viajes` DISABLE KEYS */;
INSERT INTO `vd_estados_viajes` VALUES (1,'INICIADO','Viaje Inciado'),(2,'FINALIZADO','Viaje Finalizado'),(3,'INCONCLUSO','Viaje No Finalizado');
/*!40000 ALTER TABLE `vd_estados_viajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_indicativos`
--

DROP TABLE IF EXISTS `vd_indicativos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_indicativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicativo` varchar(100) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL COMMENT 'activo o inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_indicativos`
--

LOCK TABLES `vd_indicativos` WRITE;
/*!40000 ALTER TABLE `vd_indicativos` DISABLE KEYS */;
INSERT INTO `vd_indicativos` VALUES (1,'Confianza y Seguridad',NULL),(2,'Estado del vehiculo',NULL),(3,'Cortesia',NULL),(4,'Conduccion',NULL);
/*!40000 ALTER TABLE `vd_indicativos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_lugares_frecuentes`
--

DROP TABLE IF EXISTS `vd_lugares_frecuentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_lugares_frecuentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud` varchar(45) NOT NULL,
  `longitud` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int(11) NOT NULL,
  `estado` tinyint(1) DEFAULT '0',
  `direccion` varchar(255) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `localidad` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lugares_frecuentes_1_idx` (`usuario_id`),
  CONSTRAINT `fk_lugares_frecuentes_1` FOREIGN KEY (`usuario_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_lugares_frecuentes`
--

LOCK TABLES `vd_lugares_frecuentes` WRITE;
/*!40000 ALTER TABLE `vd_lugares_frecuentes` DISABLE KEYS */;
INSERT INTO `vd_lugares_frecuentes` VALUES (8,'-24.2462627','-65.2176571','Mi Casa','2018-11-18 08:26:35',3,NULL,'los ceibos','285','palpala'),(9,'-24.23377722413','-65.267672538757','Casa','2018-11-18 19:33:38',1,NULL,'Cabo primero carrizo','809','alto comedero'),(10,'-24.2477094','-65.216707','Casa Tona','2018-11-18 19:33:59',1,NULL,'los pinos','173','palpala'),(11,'-24.2507694','-65.218411','casa ivi','2018-11-18 19:34:16',1,NULL,'arrayanes','248','palpala'),(12,'-24.186439085569','-65.303072667702','asdasd','2019-01-07 23:31:23',1,NULL,'asdasdasd','asdasd','');
/*!40000 ALTER TABLE `vd_lugares_frecuentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_notificaciones`
--

DROP TABLE IF EXISTS `vd_notificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_notificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notificacion_recibida` tinyint(1) DEFAULT '1' COMMENT '1: NO RECIBIDA, 2: RECIBIDA',
  `eliminado` tinyint(1) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `viaje_id` int(11) NOT NULL,
  `protector_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificaciones_1_idx` (`viaje_id`),
  KEY `fk_notificaciones_2_idx` (`protector_id`),
  CONSTRAINT `fk_notificaciones_1` FOREIGN KEY (`viaje_id`) REFERENCES `vd_viajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notificaciones_2` FOREIGN KEY (`protector_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_notificaciones`
--

LOCK TABLES `vd_notificaciones` WRITE;
/*!40000 ALTER TABLE `vd_notificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `vd_notificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_roles`
--

DROP TABLE IF EXISTS `vd_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  `nombre_alternativo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_roles`
--

LOCK TABLES `vd_roles` WRITE;
/*!40000 ALTER TABLE `vd_roles` DISABLE KEYS */;
INSERT INTO `vd_roles` VALUES (1,'USUARIO','ROLE_USUARIO'),(2,'TAXISTA','ROLE_TAXISTA');
/*!40000 ALTER TABLE `vd_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_taxis`
--

DROP TABLE IF EXISTS `vd_taxis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_taxis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patente` varchar(45) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `anio` varchar(8) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_taxis`
--

LOCK TABLES `vd_taxis` WRITE;
/*!40000 ALTER TABLE `vd_taxis` DISABLE KEYS */;
INSERT INTO `vd_taxis` VALUES (1,'FSG-008','Gol Power 1.6','VOLSKWAGEN','2006','Polarizado, en condiciones Correctas','RIO BALNCO','2018-05-01 00:00:00','12345'),(2,'LNK-997','207 Allure','Peugeot','2012','Descripcion','Remises Centro','2018-11-09 14:53:43','123456');
/*!40000 ALTER TABLE `vd_taxis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_taxis_choferes`
--

DROP TABLE IF EXISTS `vd_taxis_choferes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_taxis_choferes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxi_id` int(11) NOT NULL,
  `chofer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_taxis_choferes_1_idx` (`taxi_id`),
  KEY `fk_taxis_choferes_2_idx` (`chofer_id`),
  CONSTRAINT `fk_taxis_choferes_1` FOREIGN KEY (`taxi_id`) REFERENCES `vd_taxis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_taxis_choferes_2` FOREIGN KEY (`chofer_id`) REFERENCES `vd_choferes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_taxis_choferes`
--

LOCK TABLES `vd_taxis_choferes` WRITE;
/*!40000 ALTER TABLE `vd_taxis_choferes` DISABLE KEYS */;
INSERT INTO `vd_taxis_choferes` VALUES (1,1,1);
/*!40000 ALTER TABLE `vd_taxis_choferes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_usuarios`
--

DROP TABLE IF EXISTS `vd_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(45) NOT NULL COMMENT 'EL CORREO ES UNICO',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `token` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_usuarios`
--

LOCK TABLES `vd_usuarios` WRITE;
/*!40000 ALTER TABLE `vd_usuarios` DISABLE KEYS */;
INSERT INTO `vd_usuarios` VALUES (1,'clavayen','1234','Franco Bruno','Lavayen','clavayen@gmail.com','2018-06-01 00:00:00','Los Ceibos 285','3885130544',NULL,'123'),(2,'jvillca','1234','Jorge','Villca','jvillca@gmail.com','2018-10-10 00:00:00','Av. Fuerza Aerea 78','3885857458',NULL,'321'),(3,'rramos','1234','Rafael Aldo','Ramos','rramos@hotmail.com','2018-10-10 00:00:00','Pasaje 41 - Alto Comedero','3885741254',NULL,'345'),(4,'pchacon','1234','Pablo Gregorio','Chacon','pchacon@gmail.com','2018-10-10 00:00:00','Mina Pilquita N°45 - Santa Barbara','3885632141',NULL,'543'),(5,'mcruz','1234','Mateo Ezequiel','Cruz','mcruz@yahoo.com.ar','2018-10-10 00:00:00','Rio Turbio N° 34 - Mza 10 Lote 4 - Malvinas','3885987452',NULL,'567'),(6,'hbitancur','1234','asdasd','asdasd','hbitancur@gmail.com','2018-10-10 00:00:00','123123','123123',NULL,'765'),(8,'flavayen','1234','Franco','Beltran','flavayen@modernizacion.gob.ar','2018-11-09 12:53:13','Los Ceibos 285','3885130544',NULL,'678'),(9,'iliebeskind','1234','Ivana','Liebeskind','iliebeskind@gmail.com','2018-11-27 21:42:52','Arrayanes 248','3885747152',NULL,'876'),(10,'cbeltran','1234','cecilia','beltran','assad@asddas','2018-11-27 22:18:19','1234','1234',NULL,'asd'),(11,'jsalazar','1234','Josue','salazar','jsalzar@gmail.com','2019-02-01 04:44:35','Cabo Primero','3885130252',NULL,'dsa'),(12,'dsichrato','1234','dario','schirato','dschirato@gmail.com','2019-02-07 13:43:08','Italo Palanca','424242424',NULL,'dfg'),(13,'josuesalazar','1234','Josue','Salazar','Email@email','2019-02-07 15:05:48','Direcion','2132123',NULL,'gfd');
/*!40000 ALTER TABLE `vd_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_usuarios_protectores`
--

DROP TABLE IF EXISTS `vd_usuarios_protectores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_usuarios_protectores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `protector_id` int(11) NOT NULL,
  `relacion` varchar(45) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` tinyint(1) DEFAULT '0',
  `fecha_aceptacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_protectores_1_idx` (`usuario_id`),
  KEY `fk_usuarios_protectores_2_idx` (`protector_id`),
  CONSTRAINT `fk_usuarios_protectores_1` FOREIGN KEY (`usuario_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_protectores_2` FOREIGN KEY (`protector_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_usuarios_protectores`
--

LOCK TABLES `vd_usuarios_protectores` WRITE;
/*!40000 ALTER TABLE `vd_usuarios_protectores` DISABLE KEYS */;
INSERT INTO `vd_usuarios_protectores` VALUES (1,1,2,'Hermano','2018-10-26 11:56:06',1,NULL),(11,3,1,'Hermano','2018-11-27 21:36:19',1,NULL),(12,1,3,'hijo','2019-01-16 22:42:26',1,NULL);
/*!40000 ALTER TABLE `vd_usuarios_protectores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_usuarios_roles`
--

DROP TABLE IF EXISTS `vd_usuarios_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_usuarios_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_roles_1_idx` (`usuario_id`),
  KEY `fk_usuarios_roles_2_idx` (`rol_id`),
  CONSTRAINT `fk_usuarios_roles_1` FOREIGN KEY (`usuario_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_roles_2` FOREIGN KEY (`rol_id`) REFERENCES `vd_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_usuarios_roles`
--

LOCK TABLES `vd_usuarios_roles` WRITE;
/*!40000 ALTER TABLE `vd_usuarios_roles` DISABLE KEYS */;
INSERT INTO `vd_usuarios_roles` VALUES (1,1,1);
/*!40000 ALTER TABLE `vd_usuarios_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_usuarios_taxis`
--

DROP TABLE IF EXISTS `vd_usuarios_taxis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_usuarios_taxis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `taxi_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_taxis_1_idx` (`usuario_id`),
  KEY `fk_usuarios_taxis_2_idx` (`taxi_id`),
  CONSTRAINT `fk_usuarios_taxis_1` FOREIGN KEY (`usuario_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_taxis_2` FOREIGN KEY (`taxi_id`) REFERENCES `vd_taxis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_usuarios_taxis`
--

LOCK TABLES `vd_usuarios_taxis` WRITE;
/*!40000 ALTER TABLE `vd_usuarios_taxis` DISABLE KEYS */;
INSERT INTO `vd_usuarios_taxis` VALUES (1,1,2);
/*!40000 ALTER TABLE `vd_usuarios_taxis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_viajes`
--

DROP TABLE IF EXISTS `vd_viajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_viajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nro_viaje` varchar(45) NOT NULL,
  `latitud_inicio` varchar(45) NOT NULL,
  `longitud_inicio` varchar(45) NOT NULL,
  `latitud_fin` varchar(45) DEFAULT NULL,
  `longitud_fin` varchar(45) DEFAULT NULL,
  `canal_socket` varchar(45) NOT NULL,
  `calificacion` varchar(45) DEFAULT NULL,
  `cantidad_avisos` int(4) DEFAULT NULL COMMENT 'es para saber la cantidad de avisos que emitio la aplicacion para saber si termino el viaje',
  `estado_viaje_id` int(11) NOT NULL DEFAULT '1' COMMENT '1: INICIADO, 2: FINALIZADO, 3:INCONCLUSO',
  `usuario_id` int(11) NOT NULL,
  `taxi_id` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descripcion` varchar(150) DEFAULT NULL,
  `lugar_frecuente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_viajes_1_idx` (`usuario_id`),
  KEY `fk_viajes_2_idx` (`taxi_id`),
  KEY `fk_vd_viajes_1_idx` (`lugar_frecuente_id`),
  KEY `fk_vd_viajes_2_idx` (`estado_viaje_id`),
  CONSTRAINT `fk_vd_viajes_1` FOREIGN KEY (`lugar_frecuente_id`) REFERENCES `vd_lugares_frecuentes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vd_viajes_2` FOREIGN KEY (`estado_viaje_id`) REFERENCES `vd_estados_viajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_viajes_1` FOREIGN KEY (`usuario_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_viajes_2` FOREIGN KEY (`taxi_id`) REFERENCES `vd_taxis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_viajes`
--

LOCK TABLES `vd_viajes` WRITE;
/*!40000 ALTER TABLE `vd_viajes` DISABLE KEYS */;
INSERT INTO `vd_viajes` VALUES (6,'1542693128340973','-24.2507776','-65.220608','-24.2265991','-65.2372518','1542693128340973_1_1',NULL,NULL,2,1,1,'2018-11-20 06:52:08','',NULL),(7,'1542693541620044','-24.2462627','-65.2176571','-24.2265991','-65.2372518','1542693541620044_1_1',NULL,NULL,2,1,1,'2018-11-20 06:59:01','',NULL),(8,'1543352196276693','-24.2460773','-65.2169512','-24.2462627','-65.2176571','1543352196276693_1_1',NULL,NULL,2,1,1,'2018-11-27 21:56:36','',NULL),(9,'1552879682771841','-24.1895166212376','-65.29415108151204','-24.185817160318805','-65.31020142025716','1552879682771841_1_1',NULL,NULL,1,1,1,'2019-03-18 04:28:02','',NULL),(10,'1553115528178849','-24.2515252','-65.2204202','-24.2477094','-65.216707','1553115528178849_1_1',NULL,NULL,1,1,1,'2019-03-20 21:58:48',' Nro:173',10);
/*!40000 ALTER TABLE `vd_viajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_viajes_historial`
--

DROP TABLE IF EXISTS `vd_viajes_historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_viajes_historial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viaje_id` int(11) NOT NULL,
  `latitud` varchar(45) NOT NULL,
  `longitud` varchar(45) NOT NULL,
  `canal_socket` varchar(45) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_viajes_historial`
--

LOCK TABLES `vd_viajes_historial` WRITE;
/*!40000 ALTER TABLE `vd_viajes_historial` DISABLE KEYS */;
INSERT INTO `vd_viajes_historial` VALUES (137,9,'-24.189505632322','-65.292992367218','1552879682771841_1_1','2019-03-18 16:21:08'),(138,9,'-24.189104373196','-65.292552484939','1552879682771841_1_1','2019-03-18 16:21:09'),(139,9,'-24.188615031089','-65.292284264038','1552879682771841_1_1','2019-03-18 16:21:10'),(140,9,'-24.187734210566','-65.292348637054','1552879682771841_1_1','2019-03-18 16:21:11'),(141,9,'-24.187117632582','-65.292638315628','1552879682771841_1_1','2019-03-18 16:21:12'),(142,9,'-24.186540199703','-65.292992367218','1552879682771841_1_1','2019-03-18 16:21:13'),(143,9,'-24.186070421874','-65.293410791824','1552879682771841_1_1','2019-03-18 16:21:14'),(144,9,'-24.185395113213','-65.294247641037','1552879682771841_1_1','2019-03-18 16:21:15'),(145,9,'-24.185189583781','-65.29494501538','1552879682771841_1_1','2019-03-18 16:21:16'),(153,9,'-24.185063553275','-65.296157373855','1552879682771841_1_1','2019-03-18 16:39:46'),(154,9,'-24.185092914671','-65.296876205871','1552879682771841_1_1','2019-03-18 16:39:47'),(156,9,'-24.18508312754','-65.297466291854','1552879682771841_1_1','2019-03-18 16:40:11'),(157,9,'-24.185141850318','-65.298421158264','1552879682771841_1_1','2019-03-18 16:40:12'),(158,9,'-24.185161424571','-65.299064888427','1552879682771841_1_1','2019-03-18 16:40:14'),(159,9,'-24.185200573068','-65.299665703247','1552879682771841_1_1','2019-03-18 16:40:16'),(160,9,'-24.185220147313','-65.300084127853','1552879682771841_1_1','2019-03-18 16:40:18'),(161,9,'-24.185264189351','-65.300631298492','1552879682771841_1_1','2019-03-18 16:40:19'),(162,9,'-24.18526908291','-65.300985350082','1552879682771841_1_1','2019-03-18 16:40:23'),(163,9,'-24.185273976469','-65.301452054451','1552879682771841_1_1','2019-03-18 16:40:24'),(164,9,'-24.185318018489','-65.302149428794','1552879682771841_1_1','2019-03-18 16:40:25'),(165,9,'-24.185343442211','-65.302721123291','1552879682771841_1_1','2019-03-18 16:40:40'),(166,9,'-24.184951957135','-65.302844504906','1552879682771841_1_1','2019-03-18 16:40:41'),(167,9,'-24.184428343966','-65.302898149086','1552879682771841_1_1','2019-03-18 16:40:42'),(168,9,'-24.184035701824','-65.302907511552','1552879682771841_1_1','2019-03-18 16:40:44'),(169,9,'-24.18332406112','-65.302852392197','1552879682771841_1_1','2019-03-18 16:40:49'),(170,9,'-24.182880805555','-65.303073808511','1552879682771841_1_1','2019-03-18 16:40:51'),(171,9,'-24.182959103937','-65.303878471216','1552879682771841_1_1','2019-03-18 16:40:53'),(172,9,'-24.182939529346','-65.304532930215','1552879682771841_1_1','2019-03-18 16:40:54'),(173,9,'-24.183037402272','-65.305519983133','1552879682771841_1_1','2019-03-18 16:40:55'),(174,9,'-24.183056976848','-65.306238815149','1552879682771841_1_1','2019-03-18 16:40:56'),(175,9,'-24.183086338706','-65.306839629968','1552879682771841_1_1','2019-03-18 16:40:58'),(176,9,'-24.183076551421','-65.307429715951','1552879682771841_1_1','2019-03-18 16:40:59'),(177,9,'-24.183203786073','-65.308320209344','1552879682771841_1_1','2019-03-18 16:41:00'),(180,9,'-24.183235359563','-65.309269150171','1552879682771841_1_1','2019-03-18 18:32:50'),(181,9,'-24.183313657728','-65.310320576105','1552879682771841_1_1','2019-03-18 18:32:52'),(182,9,'-24.183291936884','-65.311430602638','1552879682771841_1_1','2019-03-18 18:32:59'),(183,9,'-24.183326192322','-65.312267451851','1552879682771841_1_1','2019-03-18 18:33:03'),(184,9,'-24.183335979588','-65.312605410187','1552879682771841_1_1','2019-03-18 18:33:05'),(185,9,'-24.183561086506','-65.312777071564','1552879682771841_1_1','2019-03-18 18:33:08'),(186,9,'-24.183938181789','-65.312773521729','1552879682771841_1_1','2019-03-18 18:35:01'),(187,9,'-24.184378605915','-65.312752064057','1552879682771841_1_1','2019-03-18 18:35:05'),(188,9,'-24.184590507518','-65.312018171576','1552879682771841_1_1','2019-03-18 18:35:27'),(189,9,'-24.184516378729','-65.311343049654','1552879682771841_1_1','2019-03-18 18:48:07'),(190,9,'-24.185177736347','-65.311111584929','1552879682771841_1_1','2019-03-18 18:48:59'),(191,9,'-24.185611888925','-65.311113145771','1552879682771841_1_1','2019-03-18 18:49:24'),(192,9,'-24.186301546952','-65.311099535581','1552879682771841_1_1','2019-03-18 18:53:30'),(193,9,'-24.186751749924','-65.311104899999','1552879682771841_1_1','2019-03-18 18:53:31'),(194,9,'-24.187182377367','-65.311078077908','1552879682771841_1_1','2019-03-18 18:53:32'),(195,9,'-24.187647257635','-65.310884958859','1552879682771841_1_1','2019-03-18 18:53:33'),(196,9,'-24.187833209267','-65.310697204228','1552879682771841_1_1','2019-03-18 18:53:34'),(197,9,'-24.188273619947','-65.310305601712','1552879682771841_1_1','2019-03-18 18:53:35'),(198,9,'-24.188508505021','-65.310139304753','1552879682771841_1_1','2019-03-18 18:53:36'),(199,9,'-24.188996060291','-65.310165473753','1552879682771841_1_1','2019-03-18 18:53:38'),(200,9,'-24.189392426358','-65.310288855368','1552879682771841_1_1','2019-03-18 18:53:40'),(201,9,'-24.189744750717','-65.310412236982','1552879682771841_1_1','2019-03-18 18:53:42'),(202,9,'-24.190168906441','-65.310583252543','1552879682771841_1_1','2019-03-19 00:29:18'),(203,9,'-24.190658242586','-65.310647625559','1552879682771841_1_1','2019-03-19 00:29:21'),(204,10,'-24.251173046498','-65.220709878574','1553115528178849_1_1','2019-03-20 21:59:03'),(205,10,'-24.251073087099','-65.220465660095','1553115528178849_1_1','2019-03-20 21:59:07'),(206,10,'-24.251068054376','-65.220981879359','1553115528178849_1_1','2019-03-20 21:59:17'),(207,10,'-24.251016698533','-65.221250100261','1553115528178849_1_1','2019-03-20 21:59:18'),(208,10,'-24.250916432304','-65.221378846294','1553115528178849_1_1','2019-03-20 21:59:19'),(209,10,'-24.250659652576','-65.221325202113','1553115528178849_1_1','2019-03-20 21:59:20'),(210,10,'-24.250429773141','-65.22127960456','1553115528178849_1_1','2019-03-20 21:59:21'),(211,10,'-24.250152409128','-65.221223278171','1553115528178849_1_1','2019-03-20 21:59:24'),(212,10,'-24.249968504887','-65.221174998409','1553115528178849_1_1','2019-03-20 21:59:26'),(213,10,'-24.249736178662','-65.221140129691','1553115528178849_1_1','2019-03-20 21:59:27'),(214,10,'-24.2496065649','-65.22108916772','1553115528178849_1_1','2019-03-20 21:59:28'),(215,10,'-24.249639579739','-65.220897389775','1553115528178849_1_1','2019-03-20 21:59:29'),(216,10,'-24.249713620036','-65.22056520671','1553115528178849_1_1','2019-03-20 21:59:39'),(217,10,'-24.2497527487','-65.220146782103','1553115528178849_1_1','2019-03-20 21:59:40'),(218,10,'-24.249791877352','-65.219900018874','1553115528178849_1_1','2019-03-20 21:59:41'),(219,10,'-24.250094877041','-65.217826366425','1553115528178849_1_1','2019-03-20 21:59:43'),(220,10,'-24.24971460068','-65.217571861449','1553115528178849_1_1','2019-03-20 21:59:47'),(221,10,'-24.250098550173','-65.217311687174','1553115528178849_1_1','2019-03-20 21:59:49'),(222,10,'-24.250421318133','-65.216673390824','1553115528178849_1_1','2019-03-20 21:59:52'),(223,10,'-24.250088725853','-65.216372983415','1553115528178849_1_1','2019-03-20 21:59:53'),(224,10,'-24.249394191199','-65.216153041775','1553115528178849_1_1','2019-03-20 21:59:56'),(225,10,'-24.248758347125','-65.216077939923','1553115528178849_1_1','2019-03-20 21:59:57'),(226,10,'-24.248034459232','-65.215884820874','1553115528178849_1_1','2019-03-20 21:59:59');
/*!40000 ALTER TABLE `vd_viajes_historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd_viajes_protectores`
--

DROP TABLE IF EXISTS `vd_viajes_protectores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_viajes_protectores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viaje_id` int(11) NOT NULL,
  `protector_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vd_viajes_protectores_1_idx` (`viaje_id`),
  KEY `fk_vd_viajes_protectores_2_idx` (`protector_id`),
  CONSTRAINT `fk_vd_viajes_protectores_1` FOREIGN KEY (`viaje_id`) REFERENCES `vd_viajes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vd_viajes_protectores_2` FOREIGN KEY (`protector_id`) REFERENCES `vd_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_viajes_protectores`
--

LOCK TABLES `vd_viajes_protectores` WRITE;
/*!40000 ALTER TABLE `vd_viajes_protectores` DISABLE KEYS */;
INSERT INTO `vd_viajes_protectores` VALUES (9,6,3),(11,7,2),(12,7,4),(13,7,3),(14,8,2),(15,8,4),(16,8,3),(17,9,2),(18,10,2),(19,10,3);
/*!40000 ALTER TABLE `vd_viajes_protectores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'viatax'
--

--
-- Dumping routines for database 'viatax'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-11  1:48:04
